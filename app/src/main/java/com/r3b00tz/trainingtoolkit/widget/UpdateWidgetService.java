package com.r3b00tz.trainingtoolkit.widget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.parser.HtmlParser;

/**
 * Created by matthew.boyer on 4/30/2015.
 */
public class UpdateWidgetService extends Service
{
	private String WOD = "Rest Day";

	@Override
	public void onStart(Intent intent, int startId) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());

		int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
		HtmlParser parser = new HtmlParser("https://crossfit.com");

		parser.openURLOnThread();
		String contents = parser.getContentsByTagAndClass("div", "content");
		WOD = parser.cleanupContent(contents);
		//Log.i("TEST widget WOD", WOD);
		for (int widgetId : allWidgetIds) {
			RemoteViews remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.appwidget);

			// Set the text
			remoteViews.setTextViewText(R.id.textDesc, WOD);

			// Register an onClickListener
			Intent clickIntent = new Intent(this.getApplicationContext(), WidgetProvider.class);

			clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

			PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.imageUpdate, pendingIntent);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
		stopSelf();

		super.onStart(intent, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


}