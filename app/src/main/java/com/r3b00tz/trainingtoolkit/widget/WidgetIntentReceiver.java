package com.r3b00tz.trainingtoolkit.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.r3b00tz.trainingtoolkit.R;

/**
 * Created by matthew.boyer on 4/30/2015.
 */
public class WidgetIntentReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		updateWidgetPictureAndButtonListener(context);

	}

	private void updateWidgetPictureAndButtonListener(Context context)
	{
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
												  R.layout.appwidget);

		// re-registering for click listener
		remoteViews.setOnClickPendingIntent(R.id.imageUpdate,
											WidgetProvider.buildButtonPendingIntent(context));
		WidgetProvider.pushWidgetUpdate(context.getApplicationContext(),
										remoteViews);
	}
}
