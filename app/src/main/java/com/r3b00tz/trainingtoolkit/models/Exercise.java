package com.r3b00tz.trainingtoolkit.models;

import org.json.JSONObject;

/**
 * Created by matthew.boyer on 12/31/2014.
 */
public class Exercise {
    private String name;
    private String description;
    private int oneRepMax;
    private int threeRepMax;
    private int fiveRepMax;
    private int tenRepMax;
    private int prValue;
    private int prGoal;
    private int lastWeightUsed;
    private int lastSetScheme;
    private int lastRepScheme;
    private int lastRoundComplete;
    private double lastPercentageResistance;
    private long lastTimeLimit;

    public Exercise() {
        this.name = "";
        this.description = "";
        this.oneRepMax = 0;
        this.threeRepMax = 0;
        this.fiveRepMax = 0;
        this.tenRepMax = 0;
        this.prValue = 0;
        this.prGoal = 0;
        this.lastWeightUsed = 0;
        this.lastSetScheme = 0;
        this.lastRepScheme = 0;
        this.lastRoundComplete = 0;
        this.lastPercentageResistance = 0.0;
        this.lastTimeLimit = 0;
    }

    public Exercise(String name) {
        this.name = name;
        this.description = "";
        this.oneRepMax = 0;
        this.threeRepMax = 0;
        this.fiveRepMax = 0;
        this.tenRepMax = 0;
        this.prValue = 0;
        this.prGoal = 0;
        this.lastWeightUsed = 0;
        this.lastSetScheme = 0;
        this.lastRepScheme = 0;
        this.lastRoundComplete = 0;
        this.lastPercentageResistance = 0.0;
        this.lastTimeLimit = 0;
    }

    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", this.name);
            obj.put("description", this.description);
            obj.put("1RM", this.oneRepMax);
            obj.put("3RM", this.threeRepMax);
            obj.put("5RM", this.fiveRepMax);
            obj.put("10RM", this.tenRepMax);
            obj.put("prValue", this.prValue);
            obj.put("prGoal", this.prGoal);
            obj.put("lastWeightUsed", this.lastWeightUsed);
            obj.put("lastSetScheme", this.lastSetScheme);
            obj.put("lastRepScheme", this.lastRepScheme);
            obj.put("lastRoundComplete", this.lastRoundComplete);
            obj.put("lastPercentageResistance", this.lastPercentageResistance);
            obj.put("lastTimeLimit", this.lastTimeLimit);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return obj;
    }

    public void toJavaObject(JSONObject obj) {
        try {
            this.setName(obj.getString("name"));
            this.setDescription(obj.getString("description"));
            this.setOneRepMax(obj.getInt("1RM"));
            this.setThreeRepMax(obj.getInt("3RM"));
            this.setFiveRepMax(obj.getInt("5RM"));
            this.setTenRepMax(obj.getInt("10RM"));
            this.setPrValue(obj.getInt("prValue"));
            this.setPrGoal(obj.getInt("prGoal"));
            this.setLastWeightUsed(obj.getInt("lastWeightUsed"));
            this.setLastSetScheme(obj.getInt("lastSetScheme"));
            this.setLastRepScheme(obj.getInt("lastRepScheme"));
            this.setLastRoundComplete(obj.getInt("lastRoundComplete"));
            this.setLastPercentageResistance(obj.getDouble("lastPercentageResistance"));
            this.setLastTimeLimit(obj.getLong("lastTimeLimit"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getLastTimeLimit() {
        return lastTimeLimit;
    }

    public void setLastTimeLimit(long lastTimeLimit) {
        this.lastTimeLimit = lastTimeLimit;
    }

    public double getLastPercentageResistance() {
        return lastPercentageResistance;
    }

    public void setLastPercentageResistance(double lastPercentageResistance) {
        this.lastPercentageResistance = lastPercentageResistance;
    }

    public int getLastRoundComplete() {
        return lastRoundComplete;
    }

    public void setLastRoundComplete(int lastRoundComplete) {
        this.lastRoundComplete = lastRoundComplete;
    }

    public int getLastRepScheme() {
        return lastRepScheme;
    }

    public void setLastRepScheme(int lastRepScheme) {
        this.lastRepScheme = lastRepScheme;
    }

    public int getLastSetScheme() {
        return lastSetScheme;
    }

    public void setLastSetScheme(int lastSetScheme) {
        this.lastSetScheme = lastSetScheme;
    }

    public int getLastWeightUsed() {
        return lastWeightUsed;
    }

    public void setLastWeightUsed(int lastWeightUsed) {
        this.lastWeightUsed = lastWeightUsed;
    }

    public int getPrGoal() {
        return prGoal;
    }

    public void setPrGoal(int prGoal) {
        this.prGoal = prGoal;
    }

    public int getPrValue() {
        return prValue;
    }

    public void setPrValue(int prValue) {
        this.prValue = prValue;
    }

    public int getTenRepMax() {
        return tenRepMax;
    }

    public void setTenRepMax(int tenRepMax) {
        this.tenRepMax = tenRepMax;
    }

    public int getFiveRepMax() {
        return fiveRepMax;
    }

    public void setFiveRepMax(int fiveRepMax) {
        this.fiveRepMax = fiveRepMax;
    }

    public int getThreeRepMax() {
        return threeRepMax;
    }

    public void setThreeRepMax(int threeRepMax) {
        this.threeRepMax = threeRepMax;
    }

    public int getOneRepMax() {
        return oneRepMax;
    }

    public void setOneRepMax(int oneRepMax) {
        this.oneRepMax = oneRepMax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasRealData() {
         if(oneRepMax == 0 && threeRepMax == 0 && fiveRepMax == 0 && tenRepMax == 0 && prValue == 0 && prGoal == 0 && lastWeightUsed == 0 && lastSetScheme == 0 && lastRepScheme == 0 && lastRoundComplete == 0 && lastPercentageResistance == 0.0 && lastTimeLimit == 0) {
             return false;
         }
        return true;
    }
}
