package com.r3b00tz.trainingtoolkit.models;

/**
 * Created by matthew.boyer on 12/31/2014.
 */
public class StatsModel {
    private String date;
    private String bodyfatPercentage;
    private String height;
    private String weight;
    private String dailyCals;
    private String dailyCarbs;
    private String dailyProtein;
    private String dailyFat;

    public StatsModel() {
        this.date = "";
        this.bodyfatPercentage = "";
        this.height = "";
        this.weight = "";

        this.dailyCals = "";
        this.dailyCarbs = "";
        this.dailyProtein = "";
        this.dailyFat = "";
    }

    public StatsModel(String date, String bodyfatPercentage, String height, String weight, String dailyCals, String dailyCarbs, String dailyProtein, String dailyFat) {
        this.date = date;
        this.bodyfatPercentage = bodyfatPercentage;
        this.height = height;
        this.weight = weight;

        this.dailyCals = dailyCals;
        this.dailyCarbs = dailyCarbs;
        this.dailyProtein = dailyProtein;
        this.dailyFat = dailyFat;
    }

    public boolean isEmptyModel() {
        if (date.equals("") && bodyfatPercentage.equals("") && height.equals("") && weight.equals("") && dailyCals.equals("") && dailyCarbs.equals("") && dailyProtein.equals("") && dailyFat.equals("")) {
            return true;
        }
        return false;
    }

    public String getBodyfatPercentage() {
        return bodyfatPercentage;
    }

    public void setBodyfatPercentage(String bodyfatPercentage) {
        this.bodyfatPercentage = bodyfatPercentage;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDailyCals() {
        return dailyCals;
    }

    public void setDailyCals(String dailyCals) {
        this.dailyCals = dailyCals;
    }

    public String getDailyCarbs() {
        return dailyCarbs;
    }

    public void setDailyCarbs(String dailyCarbs) {
        this.dailyCarbs = dailyCarbs;
    }

    public String getDailyProtein() {
        return dailyProtein;
    }

    public void setDailyProtein(String dailyProtein) {
        this.dailyProtein = dailyProtein;
    }

    public String getDailyFat() {
        return dailyFat;
    }

    public void setDailyFat(String dailyFat) {
        this.dailyFat = dailyFat;
    }

}
