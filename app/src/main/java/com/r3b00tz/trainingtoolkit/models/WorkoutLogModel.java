package com.r3b00tz.trainingtoolkit.models;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by matthew.boyer on 12/31/2014.
 */
public class WorkoutLogModel {

    private String logDate = "";
    private String logName = "";
    private String dashSepExerciseNames = "";
    private String dashSepExerciseWeight = "";
    private String dashSepExerciseReps = "";
    private String dashSepExerciseSets = "";
    private String dashSepExerciseTimeLimit = "";
    private String dashSepExerciseRounds = "";
    private String dashSepExercisePercent1RM = "";
    private String userComments = "";
    private ArrayList<String> exerciseNames = new ArrayList<>();
    private ArrayList<String> exerciseWeight = new ArrayList<>();
    private ArrayList<String> exerciseReps = new ArrayList<>();
    private ArrayList<String> exerciseSets = new ArrayList<>();
    private ArrayList<String> exerciseTimes = new ArrayList<>();
    private ArrayList<String> exerciseRounds = new ArrayList<>();
    private ArrayList<String> exercisePercentages = new ArrayList<>();

    public WorkoutLogModel(String logDate, String logName, String dashSepExerciseNames, String dashSepExerciseWeight, String dashSepExerciseReps, String dashSepExerciseSets, String dashSepExerciseRounds, String dashSepExercisePercent1RM, String dashSepExerciseTimeLimit, String userComments) {
        this.logDate = logDate;
        this.logName = logName;
        this.dashSepExerciseNames = dashSepExerciseNames;
        this.dashSepExerciseWeight = dashSepExerciseWeight;
        this.dashSepExerciseReps = dashSepExerciseReps;
        this.dashSepExerciseSets = dashSepExerciseSets;
        this.dashSepExerciseRounds = dashSepExerciseRounds;
        this.dashSepExercisePercent1RM = dashSepExercisePercent1RM;
        this.dashSepExerciseTimeLimit = dashSepExerciseTimeLimit;
        this.userComments = userComments;

        separateDashesToLists();
    }

    public WorkoutLogModel() {

    }

    private void separateDashesToLists() {
        exerciseNames = new ArrayList<>(Arrays.asList(dashSepExerciseNames.split("-")));
        exerciseWeight = new ArrayList<>(Arrays.asList(dashSepExerciseWeight.split("-")));
        exerciseReps = new ArrayList<>(Arrays.asList(dashSepExerciseReps.split("-")));
        exerciseSets = new ArrayList<>(Arrays.asList(dashSepExerciseSets.split("-")));
        exerciseTimes = new ArrayList<>(Arrays.asList(dashSepExerciseTimeLimit.split("-")));
        exerciseRounds = new ArrayList<>(Arrays.asList(dashSepExerciseRounds.split("-")));
        exercisePercentages = new ArrayList<>(Arrays.asList(dashSepExercisePercent1RM.split("-")));
    }

    public ArrayList<String> getExerciseSets() {
        return exerciseSets;
    }

    public ArrayList<String> getExerciseReps() {
        return exerciseReps;
    }

    public ArrayList<String> getExerciseWeight() {
        return exerciseWeight;
    }

    public ArrayList<String> getExerciseNames() {
        return exerciseNames;
    }

    public ArrayList<String> getExerciseTimes() {
        return exerciseTimes;
    }

    public ArrayList<String> getExerciseRounds() {
        return exerciseRounds;
    }

    public ArrayList<String> getExercisePercentages() {
        return exercisePercentages;
    }

    public String getUserComments() {
        return userComments;
    }

    public String getLogName() {
        return logName;
    }

    public String getLogDate() {
        return logDate;
    }
}
