package com.r3b00tz.trainingtoolkit.activities.programs;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;

/**
 * This controls usage of the 5/3/1 program.
 */
public class FiveThreeOne extends AppCompatActivity {
    private TextView textWorkMax;
    private EditText edit1RM;
    private TextView w1s1;
    private TextView w1s2;
    private TextView w1s3;
    private TextView w2s1;
    private TextView w2s2;
    private TextView w2s3;
    private TextView w3s1;
    private TextView w3s2;
    private TextView w3s3;
    private TextView w4s1;
    private TextView w4s2;
    private TextView w4s3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fivethreeone);

        initViews();
        setupActionBar();
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_fivethreeone));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        textWorkMax = (TextView) findViewById(R.id.textWorkingMaxValue);
        edit1RM = (EditText) findViewById(R.id.edit1RM);
        w1s1 = (TextView) findViewById(R.id.textW1S1Weight);
        w1s2 = (TextView) findViewById(R.id.textW1S2Weight);
        w1s3 = (TextView) findViewById(R.id.textW1S3Weight);
        w2s1 = (TextView) findViewById(R.id.textW2S1Weight);
        w2s2 = (TextView) findViewById(R.id.textW2S2Weight);
        w2s3 = (TextView) findViewById(R.id.textW2S3Weight);
        w3s1 = (TextView) findViewById(R.id.textW3S1Weight);
        w3s2 = (TextView) findViewById(R.id.textW3S2Weight);
        w3s3 = (TextView) findViewById(R.id.textW3S3Weight);
        w4s1 = (TextView) findViewById(R.id.textW4S1Weight);
        w4s2 = (TextView) findViewById(R.id.textW4S2Weight);
        w4s3 = (TextView) findViewById(R.id.textW4S3Weight);
    }

    @SuppressLint("SetTextI18n")
    private void calculate() {
        String full = edit1RM.getText().toString();
        if (full.equals("")) {
            Toast.makeText(FiveThreeOne.this,
                    getResources().getString(R.string.enter_weight),
                    Toast.LENGTH_SHORT).show();
        } else {
            Double d = Integer.valueOf(full) * 0.9;
            int workMax = d.intValue();
            textWorkMax.setText(Integer.toString(workMax));

            d = workMax * 0.65;
            int weight = d.intValue();
            w1s1.setText(Integer.toString(weight));
            d = workMax * 0.75;
            weight = d.intValue();
            w1s2.setText(Integer.toString(weight));
            d = workMax * 0.85;
            weight = d.intValue();
            w1s3.setText(Integer.toString(weight));

            d = workMax * 0.7;
            weight = d.intValue();
            w2s1.setText(Integer.toString(weight));
            d = workMax * 0.8;
            weight = d.intValue();
            w2s2.setText(Integer.toString(weight));
            d = workMax * 0.9;
            weight = d.intValue();
            w2s3.setText(Integer.toString(weight));

            d = workMax * 0.75;
            weight = d.intValue();
            w3s1.setText(Integer.toString(weight));
            d = workMax * 0.85;
            weight = d.intValue();
            w3s2.setText(Integer.toString(weight));
            d = workMax * 0.95;
            weight = d.intValue();
            w3s3.setText(Integer.toString(weight));

            d = workMax * 0.4;
            weight = d.intValue();
            w4s1.setText(Integer.toString(weight));
            d = workMax * 0.5;
            weight = d.intValue();
            w4s2.setText(Integer.toString(weight));
            d = workMax * 0.6;
            weight = d.intValue();
            w4s3.setText(Integer.toString(weight));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fivethreeone_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_calc).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_calc:
                calculate();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
