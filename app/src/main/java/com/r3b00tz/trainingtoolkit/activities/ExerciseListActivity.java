package com.r3b00tz.trainingtoolkit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.adapter.ExerciseAdapter;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;
//TODO verify pulling exercise actually works, add delete capability to list onLongClick
public class ExerciseListActivity extends AppCompatActivity {

    private RecyclerView exerciseRecycler;
    private LinearLayoutManager layoutManager;
    private ExerciseAdapter exerciseAdapter;
    private ArrayList<String> exerciseNames = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);

        setupActionBar();
        fetchData();
        initViews();
    }

    private void fetchData() {
        DBAdapter dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        exerciseNames = dbAdapter.getAllExerciseNames();
        dbAdapter.close();
    }

    private void initViews() {
        exerciseRecycler = (RecyclerView) findViewById(R.id.exerciseRecycler);
        layoutManager = new LinearLayoutManager(this);
        exerciseRecycler.setLayoutManager(layoutManager);
        exerciseAdapter = new ExerciseAdapter(exerciseNames);
        exerciseRecycler.setAdapter(exerciseAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseListActivity.this, AddNewExercise.class);
                startActivity(intent);
            }
        });
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchData();
        exerciseAdapter.notifyDataSetChanged();
    }
}
