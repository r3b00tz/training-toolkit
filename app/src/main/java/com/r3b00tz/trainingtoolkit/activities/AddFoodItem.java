package com.r3b00tz.trainingtoolkit.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.dialog.FoodLogChooser;

/**
 * This class controls adding a food item to the foodlog. It then saves it to
 * the users CustomFood table so they don't have to enter it again. Created by
 * Matthew Boyer on 7/22/13.
 */
public class AddFoodItem extends AppCompatActivity {
    private DBAdapter dbAdapter = new DBAdapter(this);
    private Cursor cursor;
    private String date = "";
    private EditText itemName;
    private Dialog dialog;
    private TextView itemNameText;
    private TextView calText;
    private TextView proText;
    private TextView carbText;
    private TextView fatText;
    private Spinner whole;
    private Spinner fraction;
    private String chosenName;
    private float fcals;
    private String chosenCals;
    private float fcarbs;
    private String chosenCarbs;
    private float fprot;
    private String chosenProt;
    private float ffat;
    private String chosenFat;
    private String foodName;
    private String calories;
    private String protein;
    private String carbs;
    private String fat;
    private String one;
    private String formatcals;
    private String formatprots;
    private String formatcarbs;
    private String formatfats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food_item);
        try {
            date = getIntent().getStringExtra("date");
        } catch (Exception e) {
            date = null;
        }
        setupActionBar();
        showFoodChooser();
    }

    private void showFoodChooser() {
        FragmentManager fm = getFragmentManager();
        FoodLogChooser chooser = new FoodLogChooser(this, date);
        chooser.show(fm, getString(R.string.foodchoosertitle));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addfood_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_save).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveToFoodLog();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveToFoodLog() {
        itemName = (EditText) findViewById(R.id.editItemName);
        EditText editCals = (EditText) findViewById(R.id.editCalories);
        EditText editProt = (EditText) findViewById(R.id.editProtein);
        EditText editCarb = (EditText) findViewById(R.id.editCarbs);
        EditText editFat = (EditText) findViewById(R.id.editFat);

        chosenName = itemName.getText().toString();
        chosenName = chosenName.trim();// Remove whitespace so we're only
        // matching characters that matter
        dbAdapter.open();
        Cursor cursor = dbAdapter.getAllCustomFood();
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            if (chosenName.equals(cursor.getString(1))) {
                // food already exists load it in dialog and then save to
                // Foodlog
                chosenCals = editCals.getText().toString();
                chosenCarbs = editCarb.getText().toString();
                chosenProt = editProt.getText().toString();
                chosenFat = editFat.getText().toString();

                // trim extra whitespace
                chosenCals = chosenCals.trim();
                chosenCarbs = chosenCarbs.trim();
                chosenProt = chosenProt.trim();
                chosenFat = chosenFat.trim();

                cursor.close();
                dbAdapter.close();
                showFoodDialog();
            } else if (cursor.isLast()) {
                // it's new so save to CustomFood table and load to foodlog
                chosenCals = editCals.getText().toString();
                chosenCarbs = editCarb.getText().toString();
                chosenProt = editProt.getText().toString();
                chosenFat = editFat.getText().toString();

                // trim extra whitespace
                chosenCals = chosenCals.trim();
                chosenCarbs = chosenCarbs.trim();
                chosenProt = chosenProt.trim();
                chosenFat = chosenFat.trim();

                cursor.close();
                dbAdapter.close();
                showNewFoodDialog();
            }
            try {
                cursor.moveToNext();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void getFoodLogData() {
        dbAdapter.open();
        cursor = dbAdapter.selectFoodLogByDate(date);
        cursor.moveToFirst();
        try {
            foodName = cursor.getString(2);
            calories = cursor.getString(3);
            protein = cursor.getString(4);
            carbs = cursor.getString(5);
            fat = cursor.getString(6);
            one = cursor.getString(0);
            cursor.close();
            dbAdapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            foodName = "";
            calories = "";
            protein = "";
            carbs = "";
            fat = "";
            one = "1";
        }
    }

    private void showNewFoodDialog() {
        getFoodLogData();
        dialog = new Dialog(AddFoodItem.this);
        dialog.setContentView(R.layout.addexistingfood);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(params);
        dialog.setTitle("Add Item");
        dialog.show();

        itemNameText = (TextView) dialog.findViewById(R.id.textItemName);
        calText = (TextView) dialog.findViewById(R.id.textCaloriesValue);
        proText = (TextView) dialog.findViewById(R.id.textProteinValue);
        carbText = (TextView) dialog.findViewById(R.id.textCarbsValue);
        fatText = (TextView) dialog.findViewById(R.id.textFatValue);
        whole = (Spinner) dialog.findViewById(R.id.numberWholeServ);
        fraction = (Spinner) dialog.findViewById(R.id.numberFractionServ);

        itemNameText.setText(chosenName);
        calText.setText(chosenCals);
        proText.setText(chosenProt);
        carbText.setText(chosenCarbs);
        fatText.setText(chosenFat);

        if (chosenProt.equals("") || chosenCarbs.equals("")
                || chosenCals.equals("") || chosenFat.equals("")) {
            // then these fuckers are null and need set to zero
            chosenProt = "0";
            chosenCarbs = "0";
            chosenCals = "0";
            chosenFat = "0";
        }

        whole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                String parse = parent.getItemAtPosition(pos).toString();
                String fract = fraction.getSelectedItem().toString();

                float serv;
                switch (fract) {
                    case "0":
                        serv = Float.valueOf(parse) + 0.00f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/4":
                        serv = Float.valueOf(parse) + 0.25f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/3":
                        serv = Float.valueOf(parse) + 0.33f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/2":
                        serv = Float.valueOf(parse) + 0.5f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "2/3":
                        serv = Float.valueOf(parse) + 0.66f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "3/4":
                        serv = Float.valueOf(parse) + 0.75f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                }
                formatcals = String.format("%.2f", fcals);
                formatcarbs = String.format("%.2f", fcarbs);
                formatprots = String.format("%.2f", fprot);
                formatfats = String.format("%.2f", ffat);
                calText.setText(formatcals);
                carbText.setText(formatcarbs);
                proText.setText(formatprots);
                fatText.setText(formatfats);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        fraction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                String parse = whole.getSelectedItem().toString();
                String fract = parent.getSelectedItem().toString();

                float serv;
                switch (fract) {
                    case "0":
                        serv = Float.valueOf(parse) + 0.00f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/4":
                        serv = Float.valueOf(parse) + 0.25f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/3":
                        serv = Float.valueOf(parse) + 0.33f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/2":
                        serv = Float.valueOf(parse) + 0.5f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "2/3":
                        serv = Float.valueOf(parse) + 0.66f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "3/4":
                        serv = Float.valueOf(parse) + 0.75f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                }
                formatcals = String.format("%.2f", fcals);
                formatcarbs = String.format("%.2f", fcarbs);
                formatprots = String.format("%.2f", fprot);
                formatfats = String.format("%.2f", ffat);
                calText.setText(formatcals);
                carbText.setText(formatcarbs);
                proText.setText(formatprots);
                fatText.setText(formatfats);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        itemName.setText(chosenName);
        calText.setText(chosenCals);
        proText.setText(chosenProt);
        carbText.setText(chosenCarbs);
        fatText.setText(chosenFat);

        // set up button
        Button button = (Button) dialog.findViewById(R.id.buttonDone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                foodName += chosenName + "-";
                calories += formatcals + "-";
                protein += formatprots + "-";
                carbs += formatcarbs + "-";
                fat += formatfats + "-";
                String servSize = whole + " " + fraction;
                dbAdapter.open();
                cursor = dbAdapter.getAllFoodLogs();
                int row = cursor.getCount();// zero based index, subtract one to
                // get the last row which is the
                // most current date
                dbAdapter.createCustomFood(chosenName, servSize, chosenCals,
                        chosenProt, chosenCarbs, chosenFat);
                dbAdapter.updateExistingFoodLog(row, date, foodName, calories,
                        protein, carbs, fat, servSize);

                cursor.close();
                dbAdapter.close();
                dialog.dismiss();
                Intent intent = new Intent(AddFoodItem.this, FoodLog.class);
                intent.putExtra("selectedDate", date);
                startActivity(intent);
            }
        });
    }

    private void showFoodDialog() {
        getFoodLogData();
        dialog = new Dialog(AddFoodItem.this);

        dialog.setContentView(R.layout.addexistingfood);
        dialog.show();

        itemNameText = (TextView) dialog.findViewById(R.id.textItemName);
        calText = (TextView) dialog.findViewById(R.id.textCaloriesValue);
        proText = (TextView) dialog.findViewById(R.id.textProteinValue);
        carbText = (TextView) dialog.findViewById(R.id.textCarbsValue);
        fatText = (TextView) dialog.findViewById(R.id.textFatValue);
        whole = (Spinner) dialog.findViewById(R.id.numberWholeServ);
        fraction = (Spinner) dialog.findViewById(R.id.numberFractionServ);

        itemNameText.setText(chosenName);
        calText.setText(chosenCals);
        proText.setText(chosenProt);
        carbText.setText(chosenCarbs);
        fatText.setText(chosenFat);

        if (chosenProt.equals("") || chosenCarbs.equals("")
                || chosenCals.equals("") || chosenFat.equals("")) {
            // then these fuckers are null and need set to zero
            chosenProt = "0";
            chosenCarbs = "0";
            chosenCals = "0";
            chosenFat = "0";
        }

        whole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                String parse = parent.getItemAtPosition(pos).toString();
                String fract = fraction.getSelectedItem().toString();

                float serv;
                switch (fract) {
                    case "0":
                        serv = Float.valueOf(parse) + 0.00f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/4":
                        serv = Float.valueOf(parse) + 0.25f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/3":
                        serv = Float.valueOf(parse) + 0.33f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/2":
                        serv = Float.valueOf(parse) + 0.5f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "2/3":
                        serv = Float.valueOf(parse) + 0.66f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "3/4":
                        serv = Float.valueOf(parse) + 0.75f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                }
                formatcals = String.format("%.2f", fcals);
                formatcarbs = String.format("%.2f", fcarbs);
                formatprots = String.format("%.2f", fprot);
                formatfats = String.format("%.2f", ffat);
                calText.setText(formatcals);
                carbText.setText(formatcarbs);
                proText.setText(formatprots);
                fatText.setText(formatfats);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        fraction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                String parse = whole.getSelectedItem().toString();
                String fract = parent.getSelectedItem().toString();

                float serv;
                switch (fract) {
                    case "0":
                        serv = Float.valueOf(parse) + 0.00f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/4":
                        serv = Float.valueOf(parse) + 0.25f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/3":
                        serv = Float.valueOf(parse) + 0.33f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "1/2":
                        serv = Float.valueOf(parse) + 0.5f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "2/3":
                        serv = Float.valueOf(parse) + 0.66f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                    case "3/4":
                        serv = Float.valueOf(parse) + 0.75f;
                        fcals = Math.round(serv * Float.valueOf(chosenCals));
                        fcarbs = Math.round(serv * Float.valueOf(chosenCarbs));
                        fprot = Math.round(serv * Float.valueOf(chosenProt));
                        ffat = Math.round(serv * Float.valueOf(chosenFat));
                        break;
                }
                formatcals = String.format("%.2f", fcals);
                formatcarbs = String.format("%.2f", fcarbs);
                formatprots = String.format("%.2f", fprot);
                formatfats = String.format("%.2f", ffat);
                calText.setText(formatcals);
                carbText.setText(formatcarbs);
                proText.setText(formatprots);
                fatText.setText(formatfats);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        itemName.setText(chosenName);
        calText.setText(chosenCals);
        proText.setText(chosenProt);
        carbText.setText(chosenCarbs);
        fatText.setText(chosenFat);

        // set up button
        Button button = (Button) dialog.findViewById(R.id.buttonDone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                foodName += chosenName + "-";
                calories += formatcals + "-";
                protein += formatprots + "-";
                carbs += formatcarbs + "-";
                fat += formatfats + "-";
                String servSize = whole + " " + fraction;
                dbAdapter.open();
                cursor = dbAdapter.getAllFoodLogs();
                Long row = Long.valueOf(one);
                dbAdapter.updateExistingFoodLog(row, date, foodName, calories,
                        protein, carbs, fat, servSize);
                cursor.close();
                dbAdapter.close();
                Intent intent = new Intent(AddFoodItem.this, FoodLog.class);
                intent.putExtra("selectedDate", date);
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}