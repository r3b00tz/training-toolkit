package com.r3b00tz.trainingtoolkit.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.date.DateHandler;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This activity is the Main screen for the foodlog functionality. It controls
 * tracking the user's diet and updating the CustomFood and foodlog databases.
 * Created by Matthew Boyer on 6/30/13.
 */
public class FoodLog extends AppCompatActivity {
    private ListView foodList;
    private TextView logDate;
    private DBAdapter dbAdapter;
    private Cursor cursor;
    private String selectedDate;
    private ArrayList<String> foodLogNames;
    private ArrayList<String> foodCalories;
    private ArrayList<String> foodProtein;
    private ArrayList<String> foodCarbs;
    private ArrayList<String> foodFat;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> foodLogData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_log);

        // get extra if exists
        selectedDate = getIntent().getStringExtra("selectedDate");

        setupActionBar();
        initViews();
        if (selectedDate != null) {
            logDate.setText(selectedDate);
            loadPreviousLog(selectedDate);
        } else {
            DateHandler minion = new DateHandler();
            selectedDate = minion.getCurrentDateSlashed();
            logDate.setText(selectedDate);
            setFoodList();
        }
    }

    private void initViews() {
        logDate = (TextView) findViewById(R.id.textFoodlogDateValue);
        foodList = (ListView) findViewById(R.id.foodlist);
        FloatingActionButton addBut = (FloatingActionButton) findViewById(R.id.buttonAddFoodItem);

        addBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View p1) {
                Intent intent = new Intent(FoodLog.this, AddFoodItem.class);
                intent.putExtra("date", logDate.getText().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_foodlog, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_past_food_log:
                Intent intent = new Intent(FoodLog.this, ExistingFoodLogs.class);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void loadPreviousLog(String date) {
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        cursor = dbAdapter.selectFoodLogByDate(date);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            if (cursor.getString(1).equals(date)) {
                selectedDate = date;
                foodLogData = new ArrayList<>();
                foodLogData.add(cursor.getString(0));// id
                foodLogData.add(cursor.getString(1));// log date
                foodLogData.add(cursor.getString(2));// foodNames
                foodLogData.add(cursor.getString(3));// foodCalories
                foodLogData.add(cursor.getString(4));// foodProtein
                foodLogData.add(cursor.getString(5));// foodCarbs
                foodLogData.add(cursor.getString(6));// foodFat

                foodLogNames = new ArrayList<>(Arrays.asList(foodLogData.get(2).split("-")));

                foodCalories = new ArrayList<>(Arrays.asList(foodLogData.get(3).split("-")));
                foodProtein = new ArrayList<>(Arrays.asList(foodLogData.get(4).split("-")));
                foodCarbs = new ArrayList<>(Arrays.asList(foodLogData.get(5).split("-")));
                foodFat = new ArrayList<>(Arrays.asList(foodLogData.get(6).split("-")));
                foodLogData.clear();
                setTotalNumbers(foodCalories, foodProtein, foodCarbs, foodFat);

                if (foodLogNames.get(0).equals("") || foodLogNames.isEmpty()) {
                    foodLogNames.add("No entries logged for this date.");
                    foodLogNames.remove(0);
                }

                adapter = new ArrayAdapter<>(this,
                        R.layout.program_spin_item, R.id.textViewItem,
                        foodLogNames);
                foodList.setAdapter(adapter);
                foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        String message = "Calories : "
                                + foodCalories.get(position) + " Carbs : "
                                + foodCarbs.get(position) + " Protein : "
                                + foodProtein.get(position) + " Fat : "
                                + foodFat.get(position);
                        Toast.makeText(FoodLog.this, message,
                                Toast.LENGTH_SHORT).show();
                    }
                });
                foodList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView,
                                                   View view, int pos, long l) {
                        final int position = pos;
                        final Dialog dialog = new Dialog(FoodLog.this);
                        dialog.setContentView(R.layout.item_options);
                        dialog.setTitle("Choose :");
                        dialog.show();

                        ArrayList<String> options = new ArrayList<>();
                        options.add(getString(R.string.Delete));
                        options.add(getString(R.string.Cancel));
                        ListView itemList = (ListView) dialog
                                .findViewById(R.id.itemListView);
                        ArrayAdapter<String> itemAdapter = new ArrayAdapter<>(
                                FoodLog.this, R.layout.list_text,
                                R.id.list_textview, options);
                        itemList.setAdapter(itemAdapter);
                        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView,
                                                    View view, int i, long l) {
                                switch (i) {
                                    case 0:
                                        deleteFoodItem(position);
                                        dialog.dismiss();
                                        break;
                                    case 1:
                                        dialog.dismiss();
                                        break;
                                }
                            }
                        });
                        return false;
                    }
                });
                dbAdapter.close();
            } else if (cursor.isLast() && !cursor.getString(1).equals(date)) {
                if (foodLogNames == null) {
                    foodLogNames = new ArrayList<>();
                    foodLogNames.add(getResources().getString(R.string.no_entries_for_date));
                }
                if (foodLogNames.get(0).equals("") || foodLogNames.isEmpty()) {
                    foodLogNames.add(getResources().getString(R.string.no_entries_for_date));
                    foodLogNames.remove(0);
                }
                adapter = new ArrayAdapter<>(this,
                        R.layout.program_spin_item, R.id.textViewItem,
                        foodLogNames);
                foodList.setAdapter(adapter);
                foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // Do nothing since there's nothing to act upon.
                    }
                });
                dbAdapter.close();
            }
            cursor.moveToNext();
        }
    }

    private void deleteFoodItem(int position) {
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        cursor = dbAdapter.selectFoodLogByDate(selectedDate);
        cursor.moveToFirst();
        String names = "";
        String cals = "";
        String prot = "";
        String carbs = "";
        String fat = "";
        long row;

        foodLogData = new ArrayList<>();
        foodLogData.add(cursor.getString(0));// id
        foodLogData.add(cursor.getString(1));// log date
        foodLogData.add(cursor.getString(2));// foodNames
        foodLogData.add(cursor.getString(3));// foodCalories
        foodLogData.add(cursor.getString(4));// foodProtein
        foodLogData.add(cursor.getString(5));// foodCarbs
        foodLogData.add(cursor.getString(6));// foodFat

        foodLogNames = new ArrayList<>(Arrays.asList(foodLogData.get(2).split("-")));
        foodCalories = new ArrayList<>(Arrays.asList(foodLogData.get(3).split("-")));
        foodProtein = new ArrayList<>(Arrays.asList(foodLogData.get(4).split("-")));
        foodCarbs = new ArrayList<>(Arrays.asList(foodLogData.get(5).split("-")));
        foodFat = new ArrayList<>(Arrays.asList(foodLogData.get(6).split("-")));

        foodLogNames.remove(position);
        foodCalories.remove(position);
        foodProtein.remove(position);
        foodCarbs.remove(position);
        foodFat.remove(position);

        row = Long.valueOf(foodLogData.get(0));
        for (int i = 0; i < foodLogNames.size(); i++) {
            names += foodLogNames.get(i) + "-";
            cals += foodCalories.get(i) + "-";
            prot += foodProtein.get(i) + "-";
            carbs += foodCarbs.get(i) + "-";
            fat += foodFat.get(i) + "-";
        }
        dbAdapter.updateExistingFoodLog(row, selectedDate, names, cals, prot,
                carbs, fat, "");
        dbAdapter.close();
        loadPreviousLog(selectedDate);
    }

    private void setFoodList() {
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        if (dbAdapter.doesFoodLogExist(selectedDate)) {
            dbAdapter.close();
            loadPreviousLog(selectedDate);
        } else {
            //new log, should be blank
            String[] emptyListContent = {getResources().getString(R.string.no_entries_for_date)};
            // create a new empty log with the date
            dbAdapter.createFoodLog(selectedDate, "", "", "", "", "", "");
            adapter = new ArrayAdapter<>(this,
                    R.layout.program_spin_item, R.id.textViewItem,
                    emptyListContent);
            foodList.setAdapter(adapter);
            foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // Do nothing since there's nothing to act upon.
                }
            });
            dbAdapter.close();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setTotalNumbers(ArrayList<String> cals,
                                 ArrayList<String> prot, ArrayList<String> carbs,
                                 ArrayList<String> fat) {
        TextView totalCals = (TextView) findViewById(R.id.foodlog_calorie_value);
        TextView totalProt = (TextView) findViewById(R.id.foodlog_protein_value);
        TextView totalCarb = (TextView) findViewById(R.id.foodlog_carb_value);
        TextView totalFat = (TextView) findViewById(R.id.foodlog_fat_value);
        float cal = 0;
        float pro = 0;
        float car = 0;
        float fa = 0;

        for (int i = 0; i < cals.size(); i++) {
            if (!cals.get(i).equals("")) {
                cal += Float.valueOf(cals.get(i));
                pro += Float.valueOf(prot.get(i));
                car += Float.valueOf(carbs.get(i));
                fa += Float.valueOf(fat.get(i));
            }
        }
        totalCals.setText(Float.toString(cal));
        totalProt.setText(Float.toString(pro));
        totalCarb.setText(Float.toString(car));
        totalFat.setText(Float.toString(fa));
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}