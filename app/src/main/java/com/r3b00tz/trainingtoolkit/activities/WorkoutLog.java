package com.r3b00tz.trainingtoolkit.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.database.ExerciseCursorAdapter;
import com.r3b00tz.trainingtoolkit.database.ExerciseCursorLoader;
import com.r3b00tz.trainingtoolkit.date.DateHandler;
import com.r3b00tz.trainingtoolkit.models.Exercise;
import com.r3b00tz.trainingtoolkit.models.WorkoutLogModel;

import java.util.ArrayList;

/**
 * Created by Matthew on 6/30/13.
 */

public class WorkoutLog extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private DBAdapter dbAdapter = new DBAdapter(this);
    private TextView date;
    private EditText workoutName;
    private ListView exerciseList;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> exercises = new ArrayList<>();
    private ArrayList<String> exercisesUsedList = new ArrayList<>();

    private String todayDate;
    private Spinner exerciseSpin;
    private EditText editsets;
    private EditText editreps;
    private EditText weight;
    private EditText editTime;
    private EditText editRounds;
    private EditText editPercent;

    private ArrayList<String> repsList = new ArrayList<>();
    private ArrayList<String> setsList = new ArrayList<>();
    private ArrayList<String> weightList = new ArrayList<>();
    private ArrayList<String> timeList = new ArrayList<>();
    private ArrayList<String> roundList = new ArrayList<>();
    private ArrayList<String> percentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workoutlog);

        DateHandler dateMinion = new DateHandler();
        todayDate = dateMinion.getCurrentDateSlashed();
        date = (TextView) findViewById(R.id.textDate);
        date.setText(todayDate);
        // get extra if exists
        String selectedDate = getIntent().getStringExtra("selectedDate");
        if (selectedDate != null) {
            date.setText(selectedDate);
        }

        setupActionBar();
        initViews();
        initBlankWorkoutLog();
        getExerciseData();
    }

    private void initViews() {
        workoutName = (EditText) findViewById(R.id.editWorkoutName);
        exerciseSpin = (Spinner) findViewById(R.id.spinnerExerciseName);
        exerciseList = (ListView) findViewById(R.id.listExerciseEditable);
        editreps = (EditText) findViewById(R.id.editRepsTime);
        editsets = (EditText) findViewById(R.id.editSetsSeries);
        editTime = (EditText) findViewById(R.id.editTime);
        editRounds = (EditText) findViewById(R.id.editRounds);
        editPercent = (EditText) findViewById(R.id.editPercentage);
        weight = (EditText) findViewById(R.id.editWeightTotal);
        FloatingActionButton addBut = (FloatingActionButton) findViewById(R.id.buttonAddExercise);

        addBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editreps.getText().toString().equals("")
                        || editsets.getText().toString().equals("")
                        || weight.getText().toString().equals("")) {
                    Toast.makeText(
                            WorkoutLog.this,
                            getResources().getString(
                                    R.string.enter_exercise_info),
                            Toast.LENGTH_SHORT).show();
                } else {
                    exercisesUsedList.add(((Cursor) exerciseSpin.getSelectedItem()).getString(1));
                    if (exercisesUsedList.get(0).equals("None")) {
                        exercisesUsedList.remove(0);
                    }
                    repsList.add(editreps.getText().toString());
                    setsList.add(editsets.getText().toString());
                    weightList.add(weight.getText().toString());
                    timeList.add((!editTime.getText().toString().equals("")) ? editTime.getText().toString() : "0");
                    roundList.add((!editRounds.getText().toString().equals("")) ? editRounds.getText().toString() : "0");
                    percentList.add((!editPercent.getText().toString().equals("")) ? editPercent.getText().toString() : "0");

                    adapter.notifyDataSetChanged();
                    editreps.setText("");
                    editsets.setText("");
                    weight.setText("");
                    editTime.setText("");
                    editRounds.setText("");
                    editPercent.setText("");
                }
            }
        });

        exerciseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String dataString = getResources().getString(R.string.exercise) + ": " + exercises.get(position) + ": " + getResources().getString(R.string.reps) + ": " + repsList.get(position) + " " + getResources().getString(R.string.sets) + ": " + setsList.get(position) + " " + getResources().getString(R.string.weight) + ": " + weightList + " " + getResources().getString(R.string.time_limit) + ": " + timeList.get(position) + " " + getResources().getString(R.string.rounds) + ": " + roundList.get(position) + " " + getResources().getString(R.string.percent1rm) + ": " + percentList.get(position);
                Toast.makeText(WorkoutLog.this, dataString, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * This is a custom click method for the delete button inside the listview,
     * it is set to call this in xml.
     *
     * @param v
     */
    public void clickDeleteHandler(View v) {
        // get the row the clicked button is in
        RelativeLayout vwParentRow = (RelativeLayout) v.getParent();
        final TextView child = (TextView) vwParentRow.getChildAt(0);
        int index = exercisesUsedList.indexOf(child.getText().toString());
        exercisesUsedList.remove(index);
        repsList.remove(index);
        setsList.remove(index);
        weightList.remove(index);
        timeList.remove(index);
        roundList.remove(index);
        percentList.remove(index);
        adapter.notifyDataSetChanged();
    }

    private void initBlankWorkoutLog() {
        dbAdapter.open();
        WorkoutLogModel model = dbAdapter.selectWorkoutLogByDate(todayDate);
        dbAdapter.close();
        if (model != null && model.getLogDate().equals(todayDate)) {
            try {
                exercisesUsedList = new ArrayList<>(model.getExerciseNames());
                weightList = new ArrayList<>(model.getExerciseWeight());
                repsList = new ArrayList<>(model.getExerciseReps());
                setsList = new ArrayList<>(model.getExerciseSets());
                timeList = new ArrayList<>(model.getExerciseTimes());
                roundList = new ArrayList<>(model.getExerciseRounds());
                percentList = new ArrayList<>(model.getExercisePercentages());

                setAdapter();
                workoutName.setText(model.getLogName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            setAdapter();
        }
    }

    private void setAdapter() {
        adapter = new ArrayAdapter<>(this, R.layout.workout_list_item, R.id.textViewWorkoutLog, exercisesUsedList);
        exerciseList.setAdapter(adapter);
    }

    private void getExerciseData() {
        getSupportLoaderManager().initLoader(0, null, this);
    }

    private void setExerciseData() {
        for (String exerciseName : exercisesUsedList) {
            int index = exercisesUsedList.indexOf(exerciseName);
            dbAdapter.open();
            Exercise exercise = dbAdapter.getExerciseByName(exerciseName);
            dbAdapter.close();
            exercise.setLastRepScheme(Integer.parseInt(repsList.get(index)));
            exercise.setLastSetScheme(Integer.parseInt(setsList.get(index)));
            exercise.setLastWeightUsed(Integer.parseInt(weightList.get(index)));
            exercise.setLastPercentageResistance(Integer.parseInt(percentList.get(index)));
            exercise.setLastTimeLimit(Long.parseLong(timeList.get(index)));
            exercise.setLastRoundComplete(Integer.parseInt(roundList.get(index)));

            updateExerciseAndRepMaxes(exercise, weightList.get(index));
        }
    }

    private void updateExerciseAndRepMaxes(Exercise exercise, final String weight) {
        dbAdapter.open();
        final boolean exerciseAlreadyExists = dbAdapter.doesExerciseExist(exercise.getName());

        final int weightVal = Integer.valueOf(weight);
        final int oldVal1RM = exercise.getOneRepMax();
        final int oldVal3RM = exercise.getThreeRepMax();
        final int oldVal5RM = exercise.getFiveRepMax();
        final int oldVal10RM = exercise.getTenRepMax();

        if (exercise.getLastRepScheme() == 1 && weightVal > oldVal1RM) {
            if (exerciseAlreadyExists) {
                dbAdapter.updateExerciseByName(exercise.getName(), exercise.getDescription(), Integer.toString(weightVal), Integer.toString(exercise.getThreeRepMax()), Integer.toString(exercise.getFiveRepMax()), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            } else {
                dbAdapter.createExercise(exercise.getName(), exercise.getDescription(), Integer.toString(weightVal), Integer.toString(exercise.getThreeRepMax()), Integer.toString(exercise.getFiveRepMax()), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            }
        } else if (exercise.getLastRepScheme() == 3 && weightVal > oldVal3RM) {
            if (exerciseAlreadyExists) {
                dbAdapter.updateExerciseByName(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(weightVal), Integer.toString(exercise.getFiveRepMax()), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            } else {
                dbAdapter.createExercise(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(weightVal), Integer.toString(exercise.getFiveRepMax()), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            }
        } else if (exercise.getLastRepScheme() == 5 && weightVal > oldVal5RM) {
            if (exerciseAlreadyExists) {
                dbAdapter.updateExerciseByName(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(exercise.getThreeRepMax()), Integer.toString(weightVal), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            } else {
                dbAdapter.createExercise(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(exercise.getThreeRepMax()), Integer.toString(weightVal), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            }
        } else if (exercise.getLastRepScheme() == 10 && weightVal > oldVal10RM) {
            if (exerciseAlreadyExists) {
                dbAdapter.updateExerciseByName(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(exercise.getThreeRepMax()), Integer.toString(exercise.getFiveRepMax()), Integer.toString(weightVal), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            } else {
                dbAdapter.createExercise(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(exercise.getThreeRepMax()), Integer.toString(exercise.getFiveRepMax()), Integer.toString(weightVal), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            }
        } else {
            if (dbAdapter.doesExerciseExist(exercise.getName())) {
                dbAdapter.updateExerciseByName(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(exercise.getThreeRepMax()), Integer.toString(exercise.getFiveRepMax()), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            } else {
                dbAdapter.createExercise(exercise.getName(), exercise.getDescription(), Integer.toString(exercise.getOneRepMax()), Integer.toString(exercise.getThreeRepMax()), Integer.toString(exercise.getFiveRepMax()), Integer.toString(exercise.getTenRepMax()), Integer.toString(exercise.getPrValue()), Integer.toString(exercise.getPrGoal()), Integer.toString(exercise.getLastWeightUsed()), Integer.toString(exercise.getLastSetScheme()), Integer.toString(exercise.getLastRepScheme()), Integer.toString(exercise.getLastRoundComplete()), Double.toString(exercise.getLastPercentageResistance()), Long.toString(exercise.getLastTimeLimit()));
            }
        }
        dbAdapter.close();
    }

    private void saveWorkoutLog() {
        String saveDate = date.getText().toString();
        String saveWorkout = workoutName.getText().toString();

        String saveExercises = "";
        String saveReps = "";
        String saveSets = "";
        String saveWeight = "";
        String saveTime = "";
        String savePercent = "";
        String saveRounds = "";

        // get the exercises we want to track RM for from the StatsModel db
        setExerciseData();

        for (int i = 0; i < exercisesUsedList.size(); i++) {
            saveExercises += exercisesUsedList.get(i) + "-";
            saveReps += repsList.get(i) + "-";
            saveSets += setsList.get(i) + "-";
            saveWeight += weightList.get(i) + "-";
            saveTime += timeList.get(i) + "-";
            savePercent += percentList.get(i) + "-";
            saveRounds += roundList.get(i) + "-";
        }

        // make sure there's not already one existing
        DBAdapter dbAdapter = new DBAdapter(getApplicationContext());
        try {
            dbAdapter.open();
            WorkoutLogModel workoutLogModel = dbAdapter.selectWorkoutLogByDate(saveDate);

            if (workoutLogModel != null) {
                if (saveDate.equals(workoutLogModel.getLogDate())) {
                    dbAdapter.updateWorkoutLog(saveDate, saveWorkout,
                            saveExercises, saveWeight, saveReps, saveSets, saveRounds, savePercent, saveTime, "");
                    Toast.makeText(this, getResources().getString(R.string.saved),
                            Toast.LENGTH_SHORT).show();
                } else {
                    dbAdapter.createWorkoutLog(saveDate, saveWorkout, saveExercises, saveWeight, saveReps, saveSets, saveRounds, savePercent, saveTime, "");
                    Toast.makeText(this, getResources().getString(R.string.saved),
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                dbAdapter.createWorkoutLog(saveDate, saveWorkout, saveExercises, saveWeight, saveReps, saveSets, saveRounds, savePercent, saveTime, "");
                Toast.makeText(this, getResources().getString(R.string.saved),
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            dbAdapter.createWorkoutLog(saveDate, saveWorkout, saveExercises,
                    saveWeight, saveReps, saveSets, saveRounds, savePercent, saveTime, "");
            Toast.makeText(this, getResources().getString(R.string.saved),
                    Toast.LENGTH_SHORT).show();
        } finally {
            dbAdapter.close();
        }
    }

    private void getPastLog() {
        Intent intent = new Intent(WorkoutLog.this, DateSelectionWorkout.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.workout_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_save).setVisible(true);
        menu.findItem(R.id.action_logs).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_save:
                saveWorkoutLog();
                return true;
            case R.id.action_logs:
                getPastLog();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ExerciseCursorLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        ExerciseCursorAdapter exerciseAdapter = new ExerciseCursorAdapter(this, data, true);
        exerciseSpin = (Spinner) findViewById(R.id.spinnerExerciseName);
        exerciseSpin.setAdapter(exerciseAdapter);
        ((ExerciseCursorLoader) loader).closeDB();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}