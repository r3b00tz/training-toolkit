package com.r3b00tz.trainingtoolkit.activities.programs;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;

/**
 * Controls the Russian Assault programs
 */
public class RussianAssault extends AppCompatActivity {
    private EditText editWeight;
    private TextView w1d1_ra;
    private TextView w2d1_ra;
    private TextView w3d1_ra;
    private TextView w4d1_ra;
    private TextView w5d1_ra;
    private TextView w6d1_ra;
    private TextView w1d2_ra;
    private TextView w2d2_ra;
    private TextView w3d2_ra;
    private TextView w4d2_ra;
    private TextView w5d2_ra;
    private TextView w6d2_ra;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_russianassault);

        setupActionBar();
        initViews();
    }

    private void initViews() {
        editWeight = (EditText) findViewById(R.id.editWeightValue);
        w1d1_ra = (TextView) findViewById(R.id.textW1D1_RAValue);
        w2d1_ra = (TextView) findViewById(R.id.textW2D1_RAValue);
        w3d1_ra = (TextView) findViewById(R.id.textW3D1_RAValue);
        w4d1_ra = (TextView) findViewById(R.id.textW4D1_RAValue);
        w5d1_ra = (TextView) findViewById(R.id.textW5D1_RAValue);
        w6d1_ra = (TextView) findViewById(R.id.textW6D1_RAValue);
        w1d2_ra = (TextView) findViewById(R.id.textW1D2_RAValue);
        w2d2_ra = (TextView) findViewById(R.id.textW2D2_RAValue);
        w3d2_ra = (TextView) findViewById(R.id.textW3D2_RAValue);
        w4d2_ra = (TextView) findViewById(R.id.textW4D2_RAValue);
        w5d2_ra = (TextView) findViewById(R.id.textW5D2_RAValue);
        w6d2_ra = (TextView) findViewById(R.id.textW6D2_RAValue);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_russianassault));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void calculate() {
        String full = editWeight.getText().toString();
        if (full.equals("")) {
            Toast.makeText(RussianAssault.this,
                    getResources().getString(R.string.enter_weight),
                    Toast.LENGTH_SHORT).show();
        } else {
            Double d = Integer.valueOf(full) * 0.7;// 0.7 0.75 0.8 0.85
            int seventy = d.intValue();
            d = Integer.valueOf(full) * 0.75;
            int seventyFive = d.intValue();
            d = Integer.valueOf(full) * 0.8;
            int eighty = d.intValue();
            d = Integer.valueOf(full) * 0.85;
            int eightyFive = d.intValue();

            w1d1_ra.setText(Integer.toString(seventy));
            w2d1_ra.setText(Integer.toString(eighty));
            w3d1_ra.setText(Integer.toString(seventy));
            w4d1_ra.setText(Integer.toString(eighty));
            w5d1_ra.setText(Integer.toString(seventy));
            w6d1_ra.setText(Integer.toString(eighty));

            w1d2_ra.setText(Integer.toString(seventyFive));
            w2d2_ra.setText(Integer.toString(eightyFive));
            w3d2_ra.setText(Integer.toString(seventyFive));
            w4d2_ra.setText(Integer.toString(eightyFive));
            w5d2_ra.setText(Integer.toString(seventyFive));
            w6d2_ra.setText(Integer.toString(eightyFive));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fivethreeone_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_calc).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_calc:
                calculate();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}
