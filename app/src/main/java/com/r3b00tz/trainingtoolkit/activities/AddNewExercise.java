package com.r3b00tz.trainingtoolkit.activities;

/**
 * Created by Matt on 1/21/2017.
 */

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.models.Exercise;

import java.util.concurrent.TimeUnit;

public class AddNewExercise extends AppCompatActivity {

    private Exercise currentExercise;

    private EditText editName;
    private EditText editDescription;
    private EditText edit1RM;
    private EditText edit3RM;
    private EditText edit5RM;
    private EditText edit10RM;
    private EditText editPRValue;
    private EditText editPRGoal;
    private EditText editLastWeightUsed;
    private EditText editLastSetScheme;
    private EditText editLastRepScheme;
    private EditText editLastRoundComplete;
    private EditText editLastPercentageResistance;
    private EditText editLastTimeLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_exercise);

        currentExercise = new Exercise("");
        setupActionBar();
        initViews();
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        editName = (EditText) findViewById(R.id.editNameValue);
        editDescription = (EditText) findViewById(R.id.editDescriptionValue);
        edit1RM = (EditText) findViewById(R.id.edit1RMValue);
        edit3RM = (EditText) findViewById(R.id.edit3RMValue);
        edit5RM = (EditText) findViewById(R.id.edit5RMValue);
        edit10RM = (EditText) findViewById(R.id.edit10RMValue);
        editPRValue = (EditText) findViewById(R.id.editPRValue);
        editPRGoal = (EditText) findViewById(R.id.editPRGoalValue);
        editLastWeightUsed = (EditText) findViewById(R.id.editLastWeightUsedValue);
        editLastSetScheme = (EditText) findViewById(R.id.editLastSetSchemeValue);
        editLastRepScheme = (EditText) findViewById(R.id.editLastRepSchemeValue);
        editLastRoundComplete = (EditText) findViewById(R.id.editLastRoundsValue);
        editLastPercentageResistance = (EditText) findViewById(R.id.editLastPercentageValue);
        editLastTimeLimit = (EditText) findViewById(R.id.editLastTimeLimitValue);

        editLastWeightUsed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!editLastRepScheme.getText().toString().equals("")) {
                    calculateOtherValues();
                }
            }
        });

        editLastRepScheme.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!editLastWeightUsed.getText().toString().equals("")) {
                    calculateOtherValues();
                }
            }
        });
    }

    /**
     * After last weight/reps/sets are entered auto calculates other values for the user
     */
    @SuppressLint("SetTextI18n")
    private void calculateOtherValues() {
        int weight = Integer.valueOf(editLastWeightUsed.getText().toString());
        int reps = Integer.valueOf(editLastRepScheme.getText().toString());
        int repMax;
        int threeRM;
        int fiveRM;
        int tenRM;

        switch (reps) {
            case 1:
                repMax = (int) (weight / 0.100);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("100");
                break;
            case 2:
                repMax = (int) (weight / 0.95);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("95");
                break;
            case 3:
                repMax = (int) (weight / 0.90);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("90");
                break;
            case 4:
                repMax = (int) (weight / 0.88);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("88");
                break;
            case 5:
                repMax = (int) (weight / 0.86);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("86");
                break;
            case 6:
                repMax = (int) (weight / 0.83);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("83");
                break;
            case 7:
                repMax = (int) (weight / 0.80);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("80");
                break;
            case 8:
                repMax = (int) (weight / 0.78);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("78");
                break;
            case 9:
                repMax = (int) (weight / 0.76);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("76");
                break;
            case 10:
                repMax = (int) (weight / 0.75);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("75");
                break;
            case 11:
                repMax = (int) (weight / 0.72);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("72");
                break;
            case 12:
                repMax = (int) (weight / 0.70);
                threeRM = (int) (repMax / 0.90);
                fiveRM = (int) (repMax / 0.86);
                tenRM = (int) (repMax / 0.75);
                edit1RM.setText(Integer.toString(repMax));
                edit3RM.setText(Integer.toString(threeRM));
                edit5RM.setText(Integer.toString(fiveRM));
                edit10RM.setText(Integer.toString(tenRM));
                editLastPercentageResistance.setText("70");
                break;
        }
    }

    private void saveExerciseData(final String exerciseName, final Exercise exerciseData) {
        //check if exercise name exists, if so update, if not insert
        DBAdapter exerciseDBAdapter = new DBAdapter(this);
        exerciseDBAdapter.open();
        boolean alreadyExists = exerciseDBAdapter.doesExerciseExist(exerciseName);

        if (alreadyExists) {
            exerciseDBAdapter.updateExerciseByName(exerciseName, exerciseData.getDescription(), Integer.toString(exerciseData.getOneRepMax()), Integer.toString(exerciseData.getThreeRepMax()), Integer.toString(exerciseData.getFiveRepMax()), Integer.toString(exerciseData.getTenRepMax()), Integer.toString(exerciseData.getPrValue()), Integer.toString(exerciseData.getPrGoal()), Integer.toString(exerciseData.getLastWeightUsed()), Integer.toString(exerciseData.getLastSetScheme()), Integer.toString(exerciseData.getLastRepScheme()), Integer.toString(exerciseData.getLastRoundComplete()), Double.toString(exerciseData.getLastPercentageResistance()), Long.toString(exerciseData.getLastTimeLimit()));
        } else {
            exerciseDBAdapter.createExercise(exerciseName, exerciseData.getDescription(), Integer.toString(exerciseData.getOneRepMax()), Integer.toString(exerciseData.getThreeRepMax()), Integer.toString(exerciseData.getFiveRepMax()), Integer.toString(exerciseData.getTenRepMax()), Integer.toString(exerciseData.getPrValue()), Integer.toString(exerciseData.getPrGoal()), Integer.toString(exerciseData.getLastWeightUsed()), Integer.toString(exerciseData.getLastSetScheme()), Integer.toString(exerciseData.getLastRepScheme()), Integer.toString(exerciseData.getLastRoundComplete()), Double.toString(exerciseData.getLastPercentageResistance()), Long.toString(exerciseData.getLastTimeLimit()));
        }
        exerciseDBAdapter.close();
        Toast.makeText(this, getResources().getString(R.string.exercise_saved), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_exercise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            currentExercise.setName(editName.getText().toString());
            currentExercise.setDescription(editDescription.getText().toString());

            currentExercise.setOneRepMax(Integer.valueOf((!edit1RM.getText().toString().equals("")) ? edit1RM.getText().toString() : "0"));
            currentExercise.setThreeRepMax(Integer.valueOf((!edit3RM.getText().toString().equals("")) ? edit3RM.getText().toString() : "0"));
            currentExercise.setFiveRepMax(Integer.valueOf((!edit5RM.getText().toString().equals("")) ? edit5RM.getText().toString() : "0"));
            currentExercise.setTenRepMax(Integer.valueOf((!edit10RM.getText().toString().equals("")) ? edit10RM.getText().toString() : "0"));
            currentExercise.setPrValue(Integer.valueOf((!editPRValue.getText().toString().equals("")) ? editPRValue.getText().toString() : "0"));
            currentExercise.setPrGoal(Integer.valueOf((!editPRGoal.getText().toString().equals("")) ? editPRGoal.getText().toString() : "0"));
            currentExercise.setLastWeightUsed(Integer.valueOf((!editLastWeightUsed.getText().toString().equals("")) ? editLastWeightUsed.getText().toString() : "0"));
            currentExercise.setLastSetScheme(Integer.valueOf((!editLastSetScheme.getText().toString().equals("")) ? editLastSetScheme.getText().toString() : "0"));
            currentExercise.setLastRepScheme(Integer.valueOf((!editLastRepScheme.getText().toString().equals("")) ? editLastRepScheme.getText().toString() : "0"));
            currentExercise.setLastRoundComplete(Integer.valueOf((!editLastRoundComplete.getText().toString().equals("")) ? editLastRoundComplete.getText().toString() : "0"));
            currentExercise.setLastPercentageResistance(Double.valueOf((!editLastPercentageResistance.getText().toString().equals("")) ? editLastPercentageResistance.getText().toString() : "0"));
            //Convert string min:sec to long millis

            String time = (!editLastTimeLimit.getText().toString().equals("")) ? editLastTimeLimit.getText().toString() : "0";
            try {
                long min = Integer.parseInt(time.substring(0, 2));
                long sec;
                try {
                    sec = Integer.parseInt(time.substring(3));
                } catch (Exception e) {
                    sec = 0;
                }
                long t = (min * 60L) + sec;

                long result = TimeUnit.SECONDS.toMillis(t);
                currentExercise.setLastTimeLimit(result);
            } catch(Exception e) {
                //invalid time, not entered, etc, so we don't care.
            }
            saveExerciseData(currentExercise.getName(), currentExercise);
        }

        return super.onOptionsItemSelected(item);
    }
}