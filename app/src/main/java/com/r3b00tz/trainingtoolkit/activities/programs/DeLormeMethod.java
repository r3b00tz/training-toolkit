package com.r3b00tz.trainingtoolkit.activities.programs;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;

/**
 * Controls the working of the DeLorme program
 */
public class DeLormeMethod extends AppCompatActivity {
    private EditText editWeight;
    private TextView w1d1;
    private TextView w2d1;
    private TextView w3d1;
    private TextView w1d3;
    private TextView w1d5;
    private TextView w2d5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delorme_method);

        setupActionBar();
        initViews();
    }

    private void initViews() {
        editWeight = (EditText) findViewById(R.id.edit10RMValue);
        w1d1 = (TextView) findViewById(R.id.textW1D1);
        w2d1 = (TextView) findViewById(R.id.textW2D1);
        w3d1 = (TextView) findViewById(R.id.textW3D1);
        w1d3 = (TextView) findViewById(R.id.textW1D3);
        w1d5 = (TextView) findViewById(R.id.textW1D5);
        w2d5 = (TextView) findViewById(R.id.textW2D5);

    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_delormemethod));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @SuppressLint("SetTextI18n")
    private void calculate() {
        String full = editWeight.getText().toString();
        if (full.equals("")) {
            Toast.makeText(DeLormeMethod.this,
                    getResources().getString(R.string.enter_weight),
                    Toast.LENGTH_SHORT).show();
        } else {
            Double d = Integer.valueOf(full) * 0.5;// 0.75 1.00
            int fifty = d.intValue();
            w1d1.setText(Integer.toString(fifty));

            d = Integer.valueOf(full) * 0.75;
            int sevenFive = d.intValue();
            w2d1.setText(Integer.toString(sevenFive));

            w3d1.setText(full);

            w1d3.setText(Integer.toString(fifty));

            w1d5.setText(Integer.toString(fifty));
            w2d5.setText(Integer.toString(sevenFive));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fivethreeone_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_calc).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_calc:
                calculate();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
