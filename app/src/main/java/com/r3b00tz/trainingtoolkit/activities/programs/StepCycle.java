package com.r3b00tz.trainingtoolkit.activities.programs;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;

/**
 * Created by Matthew on 7/8/13.
 */
public class StepCycle extends AppCompatActivity {
    private EditText weight5RM;
    private TextView week12;
    private TextView week34;
    private TextView week56;
    private TextView week78;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stepcycle);

        setupActionBar();
        initViews();
    }

    private void initViews() {
        weight5RM = (EditText) findViewById(R.id.edit5RMValue);
        week12 = (TextView) findViewById(R.id.textWeight12Value);
        week34 = (TextView) findViewById(R.id.textWeight34Value);
        week56 = (TextView) findViewById(R.id.textWeight56Value);
        week78 = (TextView) findViewById(R.id.textWeight78Value);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_stepcycle));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @SuppressLint("SetTextI18n")
    private void calculate() {
        String full = weight5RM.getText().toString();
        if (full.equals("")) {
            Toast.makeText(StepCycle.this,
                    getResources().getString(R.string.enter_weight),
                    Toast.LENGTH_SHORT).show();
        } else {
            int original = Integer.valueOf(full) - 20;
            week12.setText(Integer.toString(original));
            week34.setText(Integer.toString(original + 10));
            week56.setText(Integer.toString(original + 20));
            week78.setText(Integer.toString(original + 30));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fivethreeone_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_calc).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_calc:
                calculate();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}
