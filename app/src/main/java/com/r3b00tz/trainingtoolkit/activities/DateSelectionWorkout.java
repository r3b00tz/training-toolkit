package com.r3b00tz.trainingtoolkit.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.adapter.DividerItemDecoration;
import com.r3b00tz.trainingtoolkit.adapter.WorkoutLogAdapter;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.models.WorkoutLogModel;

import java.util.ArrayList;

/**
 * Created by Matthew on 8/21/13.
 */

public class DateSelectionWorkout extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private WorkoutLogAdapter workoutLogAdapter;
    private ArrayList<String> logNames = new ArrayList<>();
    private ArrayList<String> logDates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_existing_workout_log);

        setupActionBar();
        fetchWorkoutLogs();
    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.workoutLogRecycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        workoutLogAdapter = new WorkoutLogAdapter(logNames, logDates);
        recyclerView.setAdapter(workoutLogAdapter);

    }

    private void fetchWorkoutLogs() {
        DBAdapter dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        ArrayList<WorkoutLogModel> workoutLogs = dbAdapter.getAllWorkoutLogs();
        dbAdapter.close();
        for (WorkoutLogModel model : workoutLogs) {
            logNames.add(model.getLogName());
            logDates.add(model.getLogDate());
        }
        initViews();
    }

    private void setupActionBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}