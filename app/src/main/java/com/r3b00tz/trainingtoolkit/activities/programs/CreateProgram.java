package com.r3b00tz.trainingtoolkit.activities.programs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.Programs;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;

/**
 * This class is for creating new workout programs to be used as templates in
 * workout logs.
 */
//TODO rethink
public class CreateProgram extends Activity
{
	private EditText programName;
	private EditText programDescription;
	private ImageButton addListBut;
	private ListView exerciseListView;
	private Spinner exercises;
	private ArrayList<String> listOfExercises = new ArrayList<String>();
	private ArrayAdapter<String> adapter;
	private ArrayList<String> programExercises = new ArrayList<String>();
	private DBAdapter dbAdapter = new DBAdapter(this);
	private Cursor cursor;
	private ArrayAdapter<String> listAdapter;
	private ArrayList<String> programsExisting = new ArrayList<String>();
//	private AdView adMob;// admob

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_program);
		setupAdvertising();
		getExerciseData();
		initAllViews();
	}

	private void setupAdvertising()
	{
//		adMob = (AdView) this.findViewById(R.id.adView);
//		adMob.loadAd(new AdRequest());
	}

	private void initAllViews()
	{
		programName = (EditText) findViewById(R.id.editProgramName);
		programDescription = (EditText) findViewById(R.id.editProgramDescription);
		addListBut = (ImageButton) findViewById(R.id.buttonAdd);
		exerciseListView = (ListView) findViewById(R.id.listCreate);
		exercises = (Spinner) findViewById(R.id.spinnerExercises);

		adapter = new ArrayAdapter<String>(CreateProgram.this,
				R.layout.program_spin_item, R.id.textViewItem, listOfExercises);
		listAdapter = new ArrayAdapter<String>(CreateProgram.this,
				R.layout.program_spin_item, R.id.textViewItem, programExercises);
		exercises.setAdapter(adapter);
		exerciseListView.setAdapter(listAdapter);

		addListBut.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view)
			{
				programExercises.add(exercises.getSelectedItem().toString());
				listAdapter.notifyDataSetChanged();
			}
		});

	}

	private void getExerciseData()
	{
		dbAdapter.open();
		//listOfExercises = dbAdapter.getAllExercises();

		cursor = dbAdapter.getAllPrograms();
		cursor.moveToFirst();

		for(int i = 0; i < cursor.getCount(); i++)
		{
			programsExisting.add(cursor.getString(1));
			cursor.moveToNext();
		}
		dbAdapter.close();
	}

	private void saveProgram()
	{
		if(programName.getText().toString().equals("")
				|| programDescription.getText().toString().equals(""))
		{
			String message = getString(R.string.name_desc_needed);
			Toast.makeText(CreateProgram.this, message, Toast.LENGTH_SHORT)
					.show();
		}
		else if(programsExisting.contains(programName.getText().toString()))
		{
			String message = getString(R.string.program_name_exists);
			Toast.makeText(CreateProgram.this, message, Toast.LENGTH_SHORT)
					.show();
		}
		else
		{
			String name = programName.getText().toString();
			String desc = programDescription.getText().toString();
			String exercises = "";

			for(int i = 0; i < programExercises.size(); i++)
			{
				exercises += programExercises.get(i).toString() + "-";
			}
			dbAdapter.open();

			dbAdapter.createProgram(name, desc, exercises, "");// TODO figure
																// out how to do
																// workscheme
																// for user
			dbAdapter.close();

			Intent intent = new Intent(CreateProgram.this, Programs.class);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_program_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		menu.findItem(R.id.action_save).setVisible(true);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action buttons
		switch(item.getItemId())
		{
			case R.id.action_save:
				saveProgram();
				return true;
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}
}