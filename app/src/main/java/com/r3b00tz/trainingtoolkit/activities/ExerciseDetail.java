package com.r3b00tz.trainingtoolkit.activities;

/**
 * Created by Matt on 1/21/2017.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.models.Exercise;

import java.util.concurrent.TimeUnit;

public class ExerciseDetail extends AppCompatActivity {
    private boolean inEditMode;
    private MenuItem actionEdit;

    private Exercise currentExercise;
    private TextView textName;
    private TextView textDescription;
    private TextView text1RM;
    private TextView text3RM;
    private TextView text5RM;
    private TextView text10RM;
    private TextView textPRValue;
    private TextView textPRGoal;
    private TextView textLastWeightUsed;
    private TextView textLastSetScheme;
    private TextView textLastRepScheme;
    private TextView textLastRoundComplete;
    private TextView textLastPercentageResistance;
    private TextView textLastTimeLimit;

    private EditText editName;
    private EditText editDescription;
    private EditText edit1RM;
    private EditText edit3RM;
    private EditText edit5RM;
    private EditText edit10RM;
    private EditText editPRValue;
    private EditText editPRGoal;
    private EditText editLastWeightUsed;
    private EditText editLastSetScheme;
    private EditText editLastRepScheme;
    private EditText editLastRoundComplete;
    private EditText editLastPercentageResistance;
    private EditText editLastTimeLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail);

        inEditMode = getIntent().getBooleanExtra("editable", false);
        String exerciseName = getIntent().getStringExtra("exerciseName");

        loadData(exerciseName);
        setupActionBar();
        initViews();
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void loadData(final String exerciseNameLoaded) {
        try {
            DBAdapter exerciseDBAdapter = new DBAdapter(this);
            exerciseDBAdapter.open();
            Exercise exerciseFromDB = exerciseDBAdapter.getExerciseByName(exerciseNameLoaded);
            exerciseDBAdapter.close();
            currentExercise = exerciseFromDB;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Exercise failed to load from local storage",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void initViews() {
        textName = (TextView) findViewById(R.id.textNameValue);
        textDescription = (TextView) findViewById(R.id.textDescriptionValue);
        text1RM = (TextView) findViewById(R.id.text1RMValue);
        text3RM = (TextView) findViewById(R.id.text3RMValue);
        text5RM = (TextView) findViewById(R.id.text5RMValue);
        text10RM = (TextView) findViewById(R.id.text10RMValue);
        textPRValue = (TextView) findViewById(R.id.textPRValueValue);
        textPRGoal = (TextView) findViewById(R.id.textPRGoalValue);
        textLastWeightUsed = (TextView) findViewById(R.id.textLastWeightUsedValue);
        textLastSetScheme = (TextView) findViewById(R.id.textSetSchemeValue);
        textLastRepScheme = (TextView) findViewById(R.id.textLastRepsValue);
        textLastRoundComplete = (TextView) findViewById(R.id.textLastRoundValue);
        textLastPercentageResistance = (TextView) findViewById(R.id.textLastPercentageValue);
        textLastTimeLimit = (TextView) findViewById(R.id.textLastTimeLimitValue);

        editName = (EditText) findViewById(R.id.editNameValue);
        editDescription = (EditText) findViewById(R.id.editDescriptionValue);
        edit1RM = (EditText) findViewById(R.id.edit1RMValue);
        edit3RM = (EditText) findViewById(R.id.edit3RMValue);
        edit5RM = (EditText) findViewById(R.id.edit5RMValue);
        edit10RM = (EditText) findViewById(R.id.edit10RMValue);
        editPRValue = (EditText) findViewById(R.id.editPRValue);
        editPRGoal = (EditText) findViewById(R.id.editPRGoalValue);
        editLastWeightUsed = (EditText) findViewById(R.id.editLastWeightUsedValue);
        editLastSetScheme = (EditText) findViewById(R.id.editLastSetSchemeValue);
        editLastRepScheme = (EditText) findViewById(R.id.editLastRepSchemeValue);
        editLastRoundComplete = (EditText) findViewById(R.id.editLastRoundsValue);
        editLastPercentageResistance = (EditText) findViewById(R.id.editLastPercentageValue);
        editLastTimeLimit = (EditText) findViewById(R.id.editLastTimeLimitValue);

        if (currentExercise != null && currentExercise.hasRealData()) {
            textName.setText(currentExercise.getName());
            textDescription.setText(currentExercise.getDescription());
            text1RM.setText(Integer.toString(currentExercise.getOneRepMax()));
            text3RM.setText(Integer.toString(currentExercise.getThreeRepMax()));
            text5RM.setText(Integer.toString(currentExercise.getFiveRepMax()));
            text10RM.setText(Integer.toString(currentExercise.getTenRepMax()));
            textPRValue.setText(Integer.toString(currentExercise.getPrValue()));
            textPRGoal.setText(Integer.toString(currentExercise.getPrGoal()));
            textLastWeightUsed.setText(Integer.toString(currentExercise.getLastWeightUsed()));
            textLastSetScheme.setText(Integer.toString(currentExercise.getLastSetScheme()));
            textLastRepScheme.setText(Integer.toString(currentExercise.getLastRepScheme()));
            textLastRoundComplete.setText(Integer.toString(currentExercise.getLastRoundComplete()));

            textLastPercentageResistance.setText(
                    Double.toString(currentExercise.getLastPercentageResistance()));

            Long millis = currentExercise.getLastTimeLimit();

            textLastTimeLimit.setText(String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(millis)),
                    // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(millis))));

            editName.setText(currentExercise.getName());
            editDescription.setText(currentExercise.getDescription());
            edit1RM.setText(Integer.toString(currentExercise.getOneRepMax()));
            edit3RM.setText(Integer.toString(currentExercise.getThreeRepMax()));
            edit5RM.setText(Integer.toString(currentExercise.getFiveRepMax()));
            edit10RM.setText(Integer.toString(currentExercise.getTenRepMax()));
            editPRValue.setText(Integer.toString(currentExercise.getPrValue()));
            editPRGoal.setText(Integer.toString(currentExercise.getPrGoal()));
            editLastWeightUsed.setText(Integer.toString(currentExercise.getLastWeightUsed()));
            editLastSetScheme.setText(Integer.toString(currentExercise.getLastSetScheme()));
            editLastRepScheme.setText(Integer.toString(currentExercise.getLastRepScheme()));
            editLastRoundComplete.setText(Integer.toString(currentExercise.getLastRoundComplete()));

            editLastPercentageResistance.setText(
                    Double.toString(currentExercise.getLastPercentageResistance()));
            editLastTimeLimit.setText(String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(millis)),
                    // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(millis))));
        } else if (currentExercise != null) {
            if (!currentExercise.getName().equals("")) {
                editName.setText(currentExercise.getName());
            }
            inEditMode = true;
        } else {
            inEditMode = true;//currentExercise is null, maybe they deleted this prior
        }
        if (inEditMode) {
            setInEditMode();
        }

        editName = (EditText) findViewById(R.id.editNameValue);
        editDescription = (EditText) findViewById(R.id.editDescriptionValue);
        edit1RM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edit1RM.setText("");
                } else {
                    if (edit1RM.getText().toString().equals("")) {
                        edit1RM.setText("0");
                    }
                }
            }
        });
        edit3RM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edit3RM.setText("");
                } else {
                    if (edit3RM.getText().toString().equals("")) {
                        edit3RM.setText("0");
                    }
                }
            }
        });
        edit5RM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edit5RM.setText("");
                } else {
                    if (edit5RM.getText().toString().equals("")) {
                        edit5RM.setText("0");
                    }
                }
            }
        });
        edit10RM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edit10RM.setText("");
                } else {
                    if (edit10RM.getText().toString().equals("")) {
                        edit10RM.setText("0");
                    }
                }
            }
        });
        editPRValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editPRValue.setText("");
                } else {
                    if (editPRValue.getText().toString().equals("")) {
                        editPRValue.setText("0");
                    }
                }
            }
        });
        editPRGoal.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editPRGoal.setText("");
                } else {
                    if (editPRGoal.getText().toString().equals("")) {
                        editPRGoal.setText("0");
                    }
                }
            }
        });
        editLastWeightUsed.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editLastWeightUsed.setText("");
                } else {
                    if (editLastWeightUsed.getText().toString().equals("")) {
                        editLastWeightUsed.setText("0");
                    }
                }
            }
        });
        editLastSetScheme.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editLastSetScheme.setText("");
                } else {
                    if (editLastSetScheme.getText().toString().equals("")) {
                        editLastSetScheme.setText("0");
                    }
                }
            }
        });
        editLastRepScheme.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editLastRepScheme.setText("");
                } else {
                    if (editLastRepScheme.getText().toString().equals("")) {
                        editLastRepScheme.setText("0");
                    }
                }
            }
        });
        editLastRoundComplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editLastRoundComplete.setText("");
                } else {
                    if (editLastRoundComplete.getText().toString().equals("")) {
                        editLastRoundComplete.setText("0");
                    }
                }
            }
        });
        editLastPercentageResistance.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editLastPercentageResistance.setText("");
                } else {
                    if (editLastPercentageResistance.getText().toString().equals("")) {
                        editLastPercentageResistance.setText("0");
                    }
                }
            }
        });
        editLastTimeLimit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editLastTimeLimit.setText("");
                } else {
                    if (editLastTimeLimit.getText().toString().equals("")) {
                        editLastTimeLimit.setText("0");
                    }
                }
            }
        });
    }

    private void saveExerciseData(final String exerciseName, final Exercise exerciseData) {
        //check if exercise name exists, if so update, if not insert
        DBAdapter exerciseDBAdapter = new DBAdapter(this);
        exerciseDBAdapter.open();
        boolean alreadyExists = exerciseDBAdapter.doesExerciseExist(exerciseName);

        if (alreadyExists) {
            exerciseDBAdapter.updateExerciseByName(exerciseName, exerciseData.getDescription(), Integer.toString(exerciseData.getOneRepMax()), Integer.toString(exerciseData.getThreeRepMax()), Integer.toString(exerciseData.getFiveRepMax()), Integer.toString(exerciseData.getTenRepMax()), Integer.toString(exerciseData.getPrValue()), Integer.toString(exerciseData.getPrGoal()), Integer.toString(exerciseData.getLastWeightUsed()), Integer.toString(exerciseData.getLastSetScheme()), Integer.toString(exerciseData.getLastRepScheme()), Integer.toString(exerciseData.getLastRoundComplete()), Double.toString(exerciseData.getLastPercentageResistance()), Long.toString(exerciseData.getLastTimeLimit()));
        } else {
            exerciseDBAdapter.createExercise(exerciseName, exerciseData.getDescription(), Integer.toString(exerciseData.getOneRepMax()), Integer.toString(exerciseData.getThreeRepMax()), Integer.toString(exerciseData.getFiveRepMax()), Integer.toString(exerciseData.getTenRepMax()), Integer.toString(exerciseData.getPrValue()), Integer.toString(exerciseData.getPrGoal()), Integer.toString(exerciseData.getLastWeightUsed()), Integer.toString(exerciseData.getLastSetScheme()), Integer.toString(exerciseData.getLastRepScheme()), Integer.toString(exerciseData.getLastRoundComplete()), Double.toString(exerciseData.getLastPercentageResistance()), Long.toString(exerciseData.getLastTimeLimit()));
        }
        exerciseDBAdapter.close();
        Toast.makeText(this, "Exercise Saved!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercise_detail, menu);
        actionEdit = menu.findItem(R.id.action_edit);
        if (inEditMode) {
            actionEdit.setIcon(R.drawable.ic_action_save);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            if (!inEditMode) {
                setInEditMode();
                actionEdit.setIcon(R.drawable.ic_action_save);
                return true;
            } else {
                saveEditedExerciseData();

            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void setInEditMode() {
        inEditMode = true;


        textName.setVisibility(View.INVISIBLE);
        textDescription.setVisibility(View.INVISIBLE);
        text1RM.setVisibility(View.INVISIBLE);
        text3RM.setVisibility(View.INVISIBLE);
        text5RM.setVisibility(View.INVISIBLE);
        text10RM.setVisibility(View.INVISIBLE);
        textPRValue.setVisibility(View.INVISIBLE);
        textPRGoal.setVisibility(View.INVISIBLE);
        textLastWeightUsed.setVisibility(View.INVISIBLE);
        textLastSetScheme.setVisibility(View.INVISIBLE);
        textLastRepScheme.setVisibility(View.INVISIBLE);
        textLastRoundComplete.setVisibility(View.INVISIBLE);
        textLastPercentageResistance.setVisibility(View.INVISIBLE);
        textLastTimeLimit.setVisibility(View.INVISIBLE);

        editName.setVisibility(View.VISIBLE);
        editDescription.setVisibility(View.VISIBLE);
        edit1RM.setVisibility(View.VISIBLE);
        edit3RM.setVisibility(View.VISIBLE);
        edit5RM.setVisibility(View.VISIBLE);
        edit10RM.setVisibility(View.VISIBLE);
        editPRValue.setVisibility(View.VISIBLE);
        editPRGoal.setVisibility(View.VISIBLE);
        editLastWeightUsed.setVisibility(View.VISIBLE);
        editLastSetScheme.setVisibility(View.VISIBLE);
        editLastRepScheme.setVisibility(View.VISIBLE);
        editLastRoundComplete.setVisibility(View.VISIBLE);
        editLastPercentageResistance.setVisibility(View.VISIBLE);
        editLastTimeLimit.setVisibility(View.VISIBLE);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void saveEditedExerciseData() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editName.getWindowToken(), 0);
            inEditMode = false;
            currentExercise.setName(editName.getText().toString());
            currentExercise.setDescription(editDescription.getText().toString());

            currentExercise.setOneRepMax(Integer.valueOf(edit1RM.getText().toString()));
            currentExercise.setThreeRepMax(Integer.valueOf(edit3RM.getText().toString()));
            currentExercise.setFiveRepMax(Integer.valueOf(edit5RM.getText().toString()));
            currentExercise.setTenRepMax(Integer.valueOf(edit10RM.getText().toString()));
            currentExercise.setPrValue(Integer.valueOf(editPRValue.getText().toString()));
            currentExercise.setPrGoal(Integer.valueOf(editPRGoal.getText().toString()));
            currentExercise.setLastWeightUsed(
                    Integer.valueOf(editLastWeightUsed.getText().toString()));
            currentExercise.setLastSetScheme(
                    Integer.valueOf(editLastSetScheme.getText().toString()));
            currentExercise.setLastRepScheme(
                    Integer.valueOf(editLastRepScheme.getText().toString()));
            currentExercise.setLastRoundComplete(
                    Integer.valueOf(editLastRoundComplete.getText().toString()));
            currentExercise.setLastPercentageResistance(
                    Double.valueOf(editLastPercentageResistance.getText().toString()));
            //Convert string min:sec to long millis

            String time = editLastTimeLimit.getText().toString();
            long min = Integer.parseInt(time.substring(0, 2));
            long sec;
            try {
                sec = Integer.parseInt(time.substring(3));
            } catch (Exception e) {
                sec = 0;
            }
            long t = (min * 60L) + sec;

            long result = TimeUnit.SECONDS.toMillis(t);
            currentExercise.setLastTimeLimit(result);
            //Reset text view values from Exercise object
            textName.setText(currentExercise.getName());
            textDescription.setText(currentExercise.getDescription());
            text1RM.setText(Integer.toString(currentExercise.getOneRepMax()));
            text3RM.setText(Integer.toString(currentExercise.getThreeRepMax()));
            text5RM.setText(Integer.toString(currentExercise.getFiveRepMax()));
            text10RM.setText(Integer.toString(currentExercise.getTenRepMax()));
            textPRValue.setText(Integer.toString(currentExercise.getPrValue()));
            textPRGoal.setText(Integer.toString(currentExercise.getPrGoal()));
            textLastWeightUsed.setText(Integer.toString(currentExercise.getLastWeightUsed()));
            textLastSetScheme.setText(Integer.toString(currentExercise.getLastSetScheme()));
            textLastRepScheme.setText(Integer.toString(currentExercise.getLastRepScheme()));
            textLastRoundComplete.setText(
                    Integer.toString(currentExercise.getLastRoundComplete()));

            textLastPercentageResistance.setText(
                    Double.toString(currentExercise.getLastPercentageResistance()));

            Long millis = currentExercise.getLastTimeLimit();

            textLastTimeLimit.setText(String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(
                                            millis)),
                    // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(
                                            millis))));

            saveExerciseData(currentExercise.getName(), currentExercise);
            actionEdit.setIcon(R.drawable.ic_action_edit);

            textName.setVisibility(View.VISIBLE);
            textDescription.setVisibility(View.VISIBLE);
            text1RM.setVisibility(View.VISIBLE);
            text3RM.setVisibility(View.VISIBLE);
            text5RM.setVisibility(View.VISIBLE);
            text10RM.setVisibility(View.VISIBLE);
            textPRValue.setVisibility(View.VISIBLE);
            textPRGoal.setVisibility(View.VISIBLE);
            textLastWeightUsed.setVisibility(View.VISIBLE);
            textLastSetScheme.setVisibility(View.VISIBLE);
            textLastRepScheme.setVisibility(View.VISIBLE);
            textLastRoundComplete.setVisibility(View.VISIBLE);
            textLastPercentageResistance.setVisibility(View.VISIBLE);
            textLastTimeLimit.setVisibility(View.VISIBLE);

            editName.setVisibility(View.INVISIBLE);
            editDescription.setVisibility(View.INVISIBLE);
            edit1RM.setVisibility(View.INVISIBLE);
            edit3RM.setVisibility(View.INVISIBLE);
            edit5RM.setVisibility(View.INVISIBLE);
            edit10RM.setVisibility(View.INVISIBLE);
            editPRValue.setVisibility(View.INVISIBLE);
            editPRGoal.setVisibility(View.INVISIBLE);
            editLastWeightUsed.setVisibility(View.INVISIBLE);
            editLastSetScheme.setVisibility(View.INVISIBLE);
            editLastRepScheme.setVisibility(View.INVISIBLE);
            editLastRoundComplete.setVisibility(View.INVISIBLE);
            editLastPercentageResistance.setVisibility(View.INVISIBLE);
            editLastTimeLimit.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            Toast.makeText(ExerciseDetail.this, "Make sure all fields have a number to save.", Toast.LENGTH_SHORT).show();
        }
    }
}
