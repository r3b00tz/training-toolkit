package com.r3b00tz.trainingtoolkit.activities;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.adapter.DividerItemDecoration;
import com.r3b00tz.trainingtoolkit.adapter.PastWorkoutLogAdapter;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.models.WorkoutLogModel;

import java.util.ArrayList;

/**
 * Created by Matthew on 8/23/13.
 */
public class PastWorkoutLog extends AppCompatActivity {
    private String selectedDate;

    private ArrayList<String> exerciseList = new ArrayList<>();
    private ArrayList<String> repList = new ArrayList<>();
    private ArrayList<String> setList = new ArrayList<>();
    private ArrayList<String> weightList = new ArrayList<>();

    private DBAdapter dbAdapter = new DBAdapter(this);
    private String logName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_workout_log);
        setupActionBar();
        TextView date = (TextView) findViewById(R.id.textPastDate);
        try {
            selectedDate = getIntent().getStringExtra("selectedDate");
            if (selectedDate != null) {
                date.setText(selectedDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
            selectedDate = null;
        }
        getWorkoutData();
        initViews();
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getWorkoutData() {
        dbAdapter.open();
        WorkoutLogModel model = dbAdapter.selectWorkoutLogByDate(selectedDate);
        dbAdapter.close();
        try {
            logName = model.getLogName();

            exerciseList = model.getExerciseNames();
            weightList = model.getExerciseWeight();
            repList = model.getExerciseReps();
            setList = model.getExerciseSets();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(PastWorkoutLog.this,
                    getResources().getString(R.string.no_workout_log),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void initViews() {
        RecyclerView exerciseRecycler = (RecyclerView) findViewById(R.id.recyclerExerciseData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        exerciseRecycler.setLayoutManager(linearLayoutManager);
        exerciseRecycler.addItemDecoration(new DividerItemDecoration(this));
        PastWorkoutLogAdapter pastWorkoutLogAdapter = new PastWorkoutLogAdapter(exerciseList, repList, setList, weightList);
        exerciseRecycler.setAdapter(pastWorkoutLogAdapter);

        TextView workoutName = (TextView) findViewById(R.id.textWorkoutName);
        workoutName.setText(logName);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}