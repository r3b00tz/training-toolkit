package com.r3b00tz.trainingtoolkit.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.parser.HtmlParser;

/**
 * Created by Matt on 2/7/2017.
 */
//TODO finish as a tool pulling crossfit.coms' wod
public class Crossfit extends AppCompatActivity {
    private HtmlParser parser;
    private ProgressDialog progressDialog;
    private String result;
    private TextView textWOD;
    private Button visitSiteBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crossfit);

        setupActionBar();
        initViews();
        setupWOD();
    }

    private void setupWOD() {
        parser = new HtmlParser("https://crossfit.com/");
        final Handler mHandler = new Handler();
        progressDialog = ProgressDialog.show(Crossfit.this, "",
                "Loading WOD...");
        Thread t = new Thread(new Runnable() {
            public void run() {
                parser.openURLOnThread();
                String contents = parser.getContentsByTagAndClass("div",
                        "content");
                //Log.i("TEST dirty contents : ", contents);
                result = parser.cleanupContent(contents);
                //Log.i("TEST clean result : ", result);
                mHandler.post(new Runnable() {

                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }
        });
        t.start();
        while (t.isAlive()) {
            // wait for result to be populated before setting the view
        }
        textWOD.setText(result);
    }

    private void initViews() {
        textWOD = (TextView) findViewById(R.id.textWODData);
        visitSiteBut = (Button) findViewById(R.id.buttonSite);

        visitSiteBut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://crossfit.com"));
                startActivity(i);
            }
        });

    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
