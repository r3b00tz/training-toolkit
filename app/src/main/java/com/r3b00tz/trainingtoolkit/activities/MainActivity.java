package com.r3b00tz.trainingtoolkit.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;
import com.r3b00tz.trainingtoolkit.date.DateHandler;
import com.r3b00tz.trainingtoolkit.models.StatsModel;
import com.r3b00tz.trainingtoolkit.util.GlobalsConstants;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";

    private final Handler mDrawerActionHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private int mNavItemId;

    //StatsModel
    private EditText editCurrentBFPercent;
    private TextView textCurrentBFPercent;
    private EditText editCurrentWeight;
    private TextView textCurrentWeight;
    private EditText editCurrentHeight;
    private TextView textCurrentHeight;

    private EditText editCalsValue;
    private EditText editCarbsValue;
    private EditText editProteinValue;
    private EditText editFatValue;
    private TextView textCalsValue;
    private TextView textCarbsValue;
    private TextView textProteinValue;
    private TextView textFatValue;
    private TextView textDateValue;

    private StatsModel statsModelData;

    private boolean inEditMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity);

        fetchStatsData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void fetchStatsData() {
        statsModelData = new StatsModel();
        DBAdapter dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        boolean exist = dbAdapter.doesStatsExist();
        if (exist) {
            statsModelData = dbAdapter.getAllStats();
        } else {
            dbAdapter.makeAllTables();
            dbAdapter.loadInitialData();
        }
        dbAdapter.close();
        initViews();
    }

    private void initViews() {
        //handle drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // listen for navigation events
        NavigationView navigationView = (NavigationView) findViewById(R.id.drawer);
        navigationView.setNavigationItemSelectedListener(this);

        // set up the hamburger icon to open and close the drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open,
                R.string.close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigate(mNavItemId);

        textDateValue = (TextView) findViewById(R.id.textDateValue);
        editCurrentBFPercent = (EditText) findViewById(R.id.editCurrentBFPercent);
        textCurrentBFPercent = (TextView) findViewById(R.id.textCurrentBFPercent);
        editCurrentWeight = (EditText) findViewById(R.id.editCurrentWeight);
        textCurrentWeight = (TextView) findViewById(R.id.textCurrentWeight);
        editCurrentHeight = (EditText) findViewById(R.id.editCurrentHeight);
        textCurrentHeight = (TextView) findViewById(R.id.textCurrentHeight);

        editCalsValue = (EditText) findViewById(R.id.editCalsValue);
        editCarbsValue = (EditText) findViewById(R.id.editCurrentCarbs);
        editProteinValue = (EditText) findViewById(R.id.editCurrentProtein);
        editFatValue = (EditText) findViewById(R.id.editCurrentFat);
        textCalsValue = (TextView) findViewById(R.id.textCalsValue);
        textCarbsValue = (TextView) findViewById(R.id.textCurrentCarbs);
        textProteinValue = (TextView) findViewById(R.id.textCurrentProtein);
        textFatValue = (TextView) findViewById(R.id.textCurrentFat);
        measureSystem();

        if (!statsModelData.isEmptyModel()) {
            editCurrentBFPercent.setText(statsModelData.getBodyfatPercentage());
            editCurrentWeight.setText(statsModelData.getWeight());
            editCurrentHeight.setText(statsModelData.getHeight());
            textCurrentBFPercent.setText(statsModelData.getBodyfatPercentage());
            textCurrentWeight.setText(statsModelData.getWeight());
            textCurrentHeight.setText(statsModelData.getHeight());

            editCalsValue.setText(statsModelData.getDailyCals());
            editCarbsValue.setText(statsModelData.getDailyCarbs());
            editProteinValue.setText(statsModelData.getDailyProtein());
            editFatValue.setText(statsModelData.getDailyFat());
            textCalsValue.setText(statsModelData.getDailyCals());
            textCarbsValue.setText(statsModelData.getDailyCarbs());
            textProteinValue.setText(statsModelData.getDailyProtein());
            textFatValue.setText(statsModelData.getDailyFat());
            textDateValue.setText(statsModelData.getDate());
            toggleEditMode(false);
            inEditMode = false;
        } else {
            toggleEditMode(true);
            inEditMode = true;
        }
    }

    private void measureSystem() {
        if (!GlobalsConstants.UsingImperial) {
            editCurrentWeight.setHint(R.string.weight_hint_kg);
            editCurrentHeight.setHint(R.string.height_hint_cm);

        } else {
            editCurrentWeight.setHint(R.string.weight_hint);
            editCurrentHeight.setHint(R.string.height_hint);
        }
    }

    private void toggleEditMode(boolean setEditMode) {
        if (setEditMode) {
            editCurrentBFPercent.setVisibility(View.VISIBLE);
            editCurrentWeight.setVisibility(View.VISIBLE);
            editCurrentHeight.setVisibility(View.VISIBLE);
            textCurrentBFPercent.setVisibility(View.INVISIBLE);
            textCurrentWeight.setVisibility(View.INVISIBLE);
            textCurrentHeight.setVisibility(View.INVISIBLE);

            editCalsValue.setVisibility(View.VISIBLE);
            editCarbsValue.setVisibility(View.VISIBLE);
            editProteinValue.setVisibility(View.VISIBLE);
            editFatValue.setVisibility(View.VISIBLE);
            textCalsValue.setVisibility(View.INVISIBLE);
            textCarbsValue.setVisibility(View.INVISIBLE);
            textProteinValue.setVisibility(View.INVISIBLE);
            textFatValue.setVisibility(View.INVISIBLE);
        } else {
            editCurrentBFPercent.setVisibility(View.INVISIBLE);
            editCurrentWeight.setVisibility(View.INVISIBLE);
            editCurrentHeight.setVisibility(View.INVISIBLE);
            textCurrentBFPercent.setVisibility(View.VISIBLE);
            textCurrentWeight.setVisibility(View.VISIBLE);
            textCurrentHeight.setVisibility(View.VISIBLE);

            editCalsValue.setVisibility(View.INVISIBLE);
            editCarbsValue.setVisibility(View.INVISIBLE);
            editProteinValue.setVisibility(View.INVISIBLE);
            editFatValue.setVisibility(View.INVISIBLE);
            textCalsValue.setVisibility(View.VISIBLE);
            textCarbsValue.setVisibility(View.VISIBLE);
            textProteinValue.setVisibility(View.VISIBLE);
            textFatValue.setVisibility(View.VISIBLE);
        }
    }

    private void navigate(final int itemId) {
        Intent intent;
        switch (itemId) {
            case R.id.navigation_item_1:
                //  already here
                break;
            case R.id.navigation_item_2:
                intent = new Intent(this, WorkoutLog.class);
                startActivity(intent);
                break;
            case R.id.navigation_item_3:
                intent = new Intent(this, FoodLog.class);
                startActivity(intent);
                break;
            case R.id.navigation_item_4:
                intent = new Intent(this, ExerciseListActivity.class);
                startActivity(intent);
                break;
            case R.id.navigation_sub_item_1:
                intent = new Intent(this, Programs.class);
                startActivity(intent);
                break;
            case R.id.navigation_sub_item_2:
                intent = new Intent(this, Crossfit.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {
        // update highlighted item in the navigation menu
        mNavItemId = menuItem.getItemId();

        // allow some time after closing the drawer before performing real navigation
        // so the user can see what is happening
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.support.v7.appcompat.R.id.home:
                return mDrawerToggle.onOptionsItemSelected(item);
            case R.id.action_main_edit:
                if (inEditMode) {
                    toggleEditMode(false);
                    inEditMode = false;
                } else {
                    toggleEditMode(true);
                    inEditMode = true;
                }
                break;
            case R.id.action_main_save:
                if (inEditMode) {
                    editCurrentBFPercent.clearFocus();
                    editCurrentWeight.clearFocus();
                    editFatValue.clearFocus();
                    editProteinValue.clearFocus();
                    editCarbsValue.clearFocus();
                    editCalsValue.clearFocus();

                    if (!editCurrentBFPercent.getText().toString().equals("") && !editCurrentWeight.getText().toString().equals("") && !editCurrentHeight.getText().toString().equals("") && !editCalsValue.getText().toString().equals("") && !editCarbsValue.getText().toString().equals("") && !editProteinValue.getText().toString().equals("") && !editFatValue.getText().toString().equals("")) {
                        final String date = getSaveDate();

                        DBAdapter adapter = new DBAdapter(MainActivity.this);
                        adapter.open();

                        adapter.updateStat(1, date, editCurrentBFPercent.getText().toString(), editCurrentHeight.getText().toString(), editCurrentWeight.getText().toString(), editCalsValue.getText().toString(), editCarbsValue.getText().toString(), editProteinValue.getText().toString(), editFatValue.getText().toString());
                        adapter.close();

                        statsModelData.setDate(date);
                        statsModelData.setBodyfatPercentage(editCurrentBFPercent.getText().toString());
                        statsModelData.setHeight(editCurrentHeight.getText().toString());
                        statsModelData.setWeight(editCurrentWeight.getText().toString());

                        statsModelData.setDailyCals(editCalsValue.getText().toString());
                        statsModelData.setDailyCarbs(editCarbsValue.getText().toString());
                        statsModelData.setDailyProtein(editProteinValue.getText().toString());
                        statsModelData.setDailyFat(editFatValue.getText().toString());

                        textDateValue.setText(date);
                        textCurrentWeight.setText(editCurrentWeight.getText().toString());
                        textCurrentBFPercent.setText(editCurrentBFPercent.getText().toString());
                        textCurrentHeight.setText(editCurrentHeight.getText().toString());
                        textCalsValue.setText(editCalsValue.getText().toString());
                        textCarbsValue.setText(editCarbsValue.getText().toString());
                        textProteinValue.setText(editProteinValue.getText().toString());
                        textFatValue.setText(editFatValue.getText().toString());

                        toggleEditMode(false);
                        inEditMode = false;
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.saved), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.enter_values), Toast.LENGTH_SHORT).show();
                    }
                    inEditMode = false;
                }
                break;
            /* TODO metric vs imperial throughout app
            case R.id.action_main_settings:

                final CharSequence[] items = {GlobalsConstants.IMPERIAL, GlobalsConstants.METRIC};

                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.settings_imperial_metric))
                        .setSingleChoiceItems(items, ((GlobalsConstants.UsingImperial) ? 0 : 1), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                GlobalsConstants.UsingImperial = (which == 0);
                            }
                        }).setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                DBAdapter adapter = new DBAdapter(MainActivity.this);
                                adapter.open();
                                ArrayList<String> prefs = adapter.getPreference(1);
                                adapter.updatePreference(prefs.get(0), (GlobalsConstants.UsingImperial) ? GlobalsConstants.IMPERIAL : GlobalsConstants.METRIC);
                                adapter.close();
                                measureSystem();
                            }
                        }).setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //Don't care
                            }
                        }).create();
                dialog.show();
                Button pos = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                Button neg = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);

                if (pos != null && neg != null) {
                    pos.setTextColor(getResources().getColor(R.color.black));
                    neg.setTextColor(getResources().getColor(R.color.black));
                }
                break;
                */
        }
        return super.onOptionsItemSelected(item);
    }

    private String getSaveDate() {
       DateHandler minion = new DateHandler();
        return minion.getCurrentDateSlashed();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_activity, menu);

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }
}