package com.r3b00tz.trainingtoolkit.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;

/**
 * This activity shows a list of Food Items the user has previously entered in
 * the app. Upon item selection it adds it to the currently selected food log.
 * OnLongClicks the user can edit or delete a specific item from the app.
 * Created by Matthew Boyer on 7/22/13.
 */

public class ExistingFoodItems extends AppCompatActivity {
    private DBAdapter dbAdapter;
    private Cursor cursor;
    private ListView list;
    private String date = "";
    private String foodName;
    private String calories;
    private String protein;
    private String carbs;
    private String fat;
    private Spinner whole;
    private Spinner fraction;
    private TextView itemName;
    private TextView calText;
    private TextView proText;
    private TextView carbText;
    private TextView fatText;
    private String chosenName;
    private String chosenCals;
    private String chosenProt;
    private String chosenCarbs;
    private String chosenFat;
    private String chosenRow;
    private float fcals;
    private float fcarbs;
    private float fprot;
    private float ffat;
    private Dialog dialog;
    private String servSize;
    private ArrayList<String> listContent;
    private ArrayAdapter<String> adapter;
    private String formatcals;
    private String formatcarbs;
    private String formatprots;
    private String formatfats;
    private String foodLogRowID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existingfood);

        try {
            // this is passed from Foodlog back to it to keep organized
            date = getIntent().getStringExtra("date");
        } catch (Exception e) {
            date = null;
        }
        list = (ListView) findViewById(R.id.existingList);

        setupActionBar();
        setupFoodList();
    }

    private void setupFoodList() {
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
        cursor = dbAdapter.getAllCustomFood();
        cursor.moveToFirst();
        listContent = new ArrayList<>();

        if (cursor.getCount() != 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                if (cursor.getString(1) != null) {
                    listContent.add(cursor.getString(1));
                    cursor.moveToNext();
                }
            }
            cursor.close();
            dbAdapter.close();
            adapter = new ArrayAdapter<>(this, R.layout.foodlist_item,
                    R.id.textViewListItem, listContent);
            list.setAdapter(adapter);
            list.setLongClickable(true);
            adapter.notifyDataSetChanged();
            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView,
                                               View view, int pos, long l) {
                    final Dialog dialog2 = new Dialog(ExistingFoodItems.this);
                    dialog2.setContentView(R.layout.item_options);
                    dialog2.setTitle("Choose :");
                    dialog2.show();

                    final TextView selected = (TextView) view
                            .findViewById(R.id.textViewListItem);
                    final String longSelected = selected.getText().toString();
                    final int position = pos;
                    ArrayList<String> options = new ArrayList<>();
                    options.add(getString(R.string.Edit));
                    options.add(getString(R.string.Delete));
                    options.add(getString(R.string.Cancel));
                    ListView itemList = (ListView) dialog2
                            .findViewById(R.id.itemListView);

                    ArrayAdapter<String> itemAdapter = new ArrayAdapter<>(
                            ExistingFoodItems.this, R.layout.list_text,
                            R.id.list_textview, options);
                    itemList.setAdapter(itemAdapter);
                    itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView,
                                                View view, int i, long l) {
                            switch (i) {
                                case 0:
                                    fetchChosenItemData(longSelected);
                                    updateFoodItem(longSelected);
                                    dialog2.dismiss();
                                    break;
                                case 1:
                                    deleteFoodItem(longSelected, position);
                                    dialog2.dismiss();
                                    break;
                                case 2:
                                    dialog2.dismiss();
                                    break;
                            }
                        }
                    });

                    return false;
                }
            });
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    TextView chosen = (TextView) view
                            .findViewById(R.id.textViewListItem);
                    String chosenText = chosen.getText().toString();
                    fetchChosenItemData(chosenText);

                    try {
                        getExistingFoodLog();
                        showFoodDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            listContent.add(getString(R.string.foodlistempty));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    R.layout.foodlist_item, R.id.textViewListItem, listContent);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // do nothing as list is empty
                }
            });
        }
    }

    private void deleteFoodItem(String name, int position) {
        fetchChosenItemData(name);
        long rowDelete = Long.valueOf(chosenRow);
        dbAdapter.open();
        dbAdapter.deleteCustomFood(rowDelete);
        listContent.remove(position);
        adapter.notifyDataSetChanged();
        dbAdapter.close();
    }

    private void getExistingFoodLog() {
        // get the existing log entries
        dbAdapter.open();
        cursor = dbAdapter.selectFoodLogByDate(date);
        cursor.moveToFirst();

        foodLogRowID = cursor.getString(0);
        foodName = cursor.getString(2);
        calories = cursor.getString(3);
        protein = cursor.getString(4);
        carbs = cursor.getString(5);
        fat = cursor.getString(6);
        cursor.close();
        dbAdapter.close();
    }

    private void fetchChosenItemData(String name) {
        dbAdapter.open();
        Cursor specificCursor = dbAdapter.getCustomFoodByName(name);
        specificCursor.moveToFirst();
        chosenRow = specificCursor.getString(0);
        chosenName = specificCursor.getString(1);
        chosenCals = specificCursor.getString(3);
        chosenProt = specificCursor.getString(4);
        chosenCarbs = specificCursor.getString(5);
        chosenFat = specificCursor.getString(6);
        specificCursor.close();
        dbAdapter.close();
    }

    private void showFoodDialog() {
        dialog = new Dialog(ExistingFoodItems.this);
        dialog.setContentView(R.layout.addexistingfood);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(params);
        dialog.setTitle("Add Item");
        dialog.show();

        itemName = (TextView) dialog.findViewById(R.id.textItemName);
        calText = (TextView) dialog.findViewById(R.id.textCaloriesValue);
        proText = (TextView) dialog.findViewById(R.id.textProteinValue);
        carbText = (TextView) dialog.findViewById(R.id.textCarbsValue);
        fatText = (TextView) dialog.findViewById(R.id.textFatValue);
        whole = (Spinner) dialog.findViewById(R.id.numberWholeServ);
        fraction = (Spinner) dialog.findViewById(R.id.numberFractionServ);

        if (chosenProt.equals("") || chosenCarbs.equals("")
                || chosenCals.equals("") || chosenFat.equals("")) {
            // then these fuckers are null and need set to zero
            chosenProt = "0";
            chosenCarbs = "0";
            chosenCals = "0";
            chosenFat = "0";
        }

        whole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                String parse = parent.getItemAtPosition(pos).toString();
                String fract = fraction.getSelectedItem().toString();

                float serv;
                switch (fract) {
                    case "0":
                        serv = Float.valueOf(parse) + 0.00f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "1/4":
                        serv = Float.valueOf(parse) + 0.25f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "1/3":
                        serv = Float.valueOf(parse) + 0.33f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "1/2":
                        serv = Float.valueOf(parse) + 0.5f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "2/3":
                        serv = Float.valueOf(parse) + 0.66f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "3/4":
                        serv = Float.valueOf(parse) + 0.75f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                }
                formatcals = String.format("%.2f", fcals);
                formatcarbs = String.format("%.2f", fcarbs);
                formatprots = String.format("%.2f", fprot);
                formatfats = String.format("%.2f", ffat);
                calText.setText(formatcals);
                carbText.setText(formatcarbs);
                proText.setText(formatprots);
                fatText.setText(formatfats);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        fraction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("DefaultLocale")
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                String parse = whole.getSelectedItem().toString();
                String fract = parent.getSelectedItem().toString();

                float serv;
                switch (fract) {
                    case "0":
                        serv = Float.valueOf(parse) + 0.00f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "1/4":
                        serv = Float.valueOf(parse) + 0.25f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "1/3":
                        serv = Float.valueOf(parse) + 0.33f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "1/2":
                        serv = Float.valueOf(parse) + 0.5f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "2/3":
                        serv = Float.valueOf(parse) + 0.66f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                    case "3/4":
                        serv = Float.valueOf(parse) + 0.75f;
                        fcals = serv * Float.valueOf(chosenCals);
                        fcarbs = serv * Float.valueOf(chosenCarbs);
                        fprot = serv * Float.valueOf(chosenProt);
                        ffat = serv * Float.valueOf(chosenFat);
                        break;
                }
                formatcals = String.format("%.2f", fcals);
                formatcarbs = String.format("%.2f", fcarbs);
                formatprots = String.format("%.2f", fprot);
                formatfats = String.format("%.2f", ffat);
                calText.setText(formatcals);
                carbText.setText(formatcarbs);
                proText.setText(formatprots);
                fatText.setText(formatfats);

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        itemName.setText(chosenName);
        calText.setText(chosenCals);
        proText.setText(chosenProt);
        carbText.setText(chosenCarbs);
        fatText.setText(chosenFat);

        // set up button
        Button button = (Button) dialog.findViewById(R.id.buttonDone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodName += chosenName + "-";
                calories += formatcals + "-";
                protein += formatprots + "-";
                carbs += formatcarbs + "-";
                fat += formatfats + "-";
                servSize = whole + " " + fraction;
                dbAdapter.open();

                dbAdapter.updateExistingFoodLog(Long.valueOf(foodLogRowID), date, foodName, calories,
                        protein, carbs, fat, servSize);
                dbAdapter.close();

                Intent intent = new Intent(ExistingFoodItems.this,
                        FoodLog.class);
                intent.putExtra("selectedDate", date);
                startActivity(intent);
            }
        });
    }

    private void updateFoodItem(final String selected) {
        dialog = new Dialog(ExistingFoodItems.this);
        dialog.setContentView(R.layout.addfood);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(params);
        dialog.setTitle("Update Item");
        dialog.show();

        itemName = (EditText) dialog.findViewById(R.id.editItemName);
        calText = (EditText) dialog.findViewById(R.id.editCalories);
        proText = (EditText) dialog.findViewById(R.id.editProtein);
        carbText = (EditText) dialog.findViewById(R.id.editCarbs);
        fatText = (EditText) dialog.findViewById(R.id.editFat);

        if (chosenProt.equals("") || chosenCarbs.equals("")
                || chosenCals.equals("") || chosenFat.equals("")) {
            chosenProt = "0";
            chosenCarbs = "0";
            chosenCals = "0";
            chosenFat = "0";
        }

        itemName.setText(selected);
        calText.setText(chosenCals);
        proText.setText(chosenProt);
        carbText.setText(chosenCarbs);
        fatText.setText(chosenFat);

        // set up button
        Button button = (Button) dialog.findViewById(R.id.buttonDone);
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                servSize = "1";// default setting as we're entering for 1
                // serving
                chosenName = itemName.getText().toString();
                chosenCals = calText.getText().toString();
                chosenCarbs = carbText.getText().toString();
                chosenProt = proText.getText().toString();
                chosenFat = fatText.getText().toString();

                dbAdapter.open();
                int row = Integer.valueOf(chosenRow);// the row for the item
                // selected on main list
                dbAdapter.updateCustomFood(row, chosenName, servSize,
                        chosenCals, chosenProt, chosenCarbs, chosenFat);
                dbAdapter.close();
                fetchChosenItemData(chosenName);
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.summary_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            dialog.dismiss();
        } catch (Exception e) {}
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}