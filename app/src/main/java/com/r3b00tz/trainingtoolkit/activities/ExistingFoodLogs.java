package com.r3b00tz.trainingtoolkit.activities;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.adapter.DividerItemDecoration;
import com.r3b00tz.trainingtoolkit.database.FoodLogCursorLoader;
import com.r3b00tz.trainingtoolkit.database.FoodlogCursorAdapter;

/**
 * Created by Matthew on 7/23/13.
 */
public class ExistingFoodLogs extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private RecyclerView recycler;
    private FoodLogCursorLoader cursorLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_existing_food_log);
        setupActionBar();

        fetchData();
        initViews();
    }

    private void fetchData() {
        getSupportLoaderManager().initLoader(0, null, this);
    }

    private void initViews() {
        recycler = (RecyclerView) findViewById(R.id.foodLogRecycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler.addItemDecoration(new DividerItemDecoration(this));
        recycler.setLayoutManager(layoutManager);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            cursorLoader.closeDB();
        } catch(Exception e) {}

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FoodLogCursorLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        FoodlogCursorAdapter foodlogCursorAdapter = new FoodlogCursorAdapter(data);

        recycler = (RecyclerView) findViewById(R.id.foodLogRecycler);
        recycler.setAdapter(foodlogCursorAdapter);
        cursorLoader = (FoodLogCursorLoader) loader;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}