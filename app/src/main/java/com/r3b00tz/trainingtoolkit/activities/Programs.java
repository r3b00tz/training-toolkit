package com.r3b00tz.trainingtoolkit.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.programs.CreateProgram;
import com.r3b00tz.trainingtoolkit.activities.programs.CustomProgram;
import com.r3b00tz.trainingtoolkit.activities.programs.DeLormeMethod;
import com.r3b00tz.trainingtoolkit.activities.programs.FiveFiveFive;
import com.r3b00tz.trainingtoolkit.activities.programs.FiveThreeOne;
import com.r3b00tz.trainingtoolkit.activities.programs.RussianAssault;
import com.r3b00tz.trainingtoolkit.activities.programs.StepCycle;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class shows the default programs available and controls allowing the
 * user to add a new program, edit a program, create a workout log from a
 * program.
 */
public class Programs extends AppCompatActivity {
    private Spinner programNameSpin;
    private TextView programDescription;
    private EditText programDescriptionEdit;
    private ArrayList<String> programNameList = new ArrayList<>();
    private ArrayList<String> programDescriptionList = new ArrayList<>();
    private ListView exerciseList;
    private ArrayAdapter<String> adapterList;
    private ArrayList<String> exerciseNameList = new ArrayList<>();
    private ArrayList<String> exerciseNameList2 = new ArrayList<>();
    private DBAdapter dbAdapter = new DBAdapter(this);
    private Button programBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programs);
        setupActionBar();
        getAllProgramData();
        initViews();
        exerciseList.setLongClickable(false);
    }

    private void initViews() {
        programNameSpin = (Spinner) findViewById(R.id.spinnerProgramName);
        Button createBut = (Button) findViewById(R.id.buttonCreate);
        programDescription = (TextView) findViewById(R.id.textProgramDescription);
        programDescriptionEdit = (EditText) findViewById(R.id.editProgramDescription);
        Button saveBut = (Button) findViewById(R.id.buttonSave);
        Button discardBut = (Button) findViewById(R.id.buttonDiscard);
        exerciseList = (ListView) findViewById(R.id.listExercises);
        programBut = (Button) findViewById(R.id.buttonDoProgram);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.program_spin_item,
                R.id.textViewItem, programNameList);
        adapterList = new ArrayAdapter<>(this,
                R.layout.list_text, R.id.list_textview,
                exerciseNameList2);
        programNameSpin.setAdapter(adapter);
        exerciseList.setAdapter(adapterList);

        programBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = programNameSpin.getSelectedItemPosition();
                Intent intent;
                switch (i) {
                    case 0:
                        intent = new Intent(Programs.this, FiveThreeOne.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(Programs.this, FiveFiveFive.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(Programs.this, DeLormeMethod.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(Programs.this, StepCycle.class);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(Programs.this, RussianAssault.class);
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(Programs.this, RussianAssault.class);
                        startActivity(intent);
                        break;
                    case 6:
                        intent = new Intent(Programs.this, CustomProgram.class);
                        startActivity(intent);
                        break;
                    default:
                        intent = new Intent(Programs.this, FiveThreeOne.class);
                        startActivity(intent);
                        break;
                }

            }
        });
        programNameSpin
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        String parse = parent.getItemAtPosition(pos).toString();
                        loadDataForProgram(parse);
                        if (pos > 5) {
                            programBut.setVisibility(View.GONE);
                        } else {
                            programBut.setVisibility(View.VISIBLE);
                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        exerciseList
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView,
                                                   View view, int pos, long l) {
                        final Dialog dialog2 = new Dialog(Programs.this);
                        dialog2.setContentView(R.layout.item_options);
                        dialog2.setTitle("Choose :");
                        dialog2.show();

                        ArrayList<String> options = new ArrayList<>();
                        options.add(getString(R.string.Edit));
                        options.add(getString(R.string.Delete));
                        options.add(getString(R.string.Cancel));
                        ListView itemList = (ListView) dialog2
                                .findViewById(R.id.itemListView);
                        ArrayAdapter<String> itemAdapter = new ArrayAdapter<>(
                                Programs.this, R.layout.list_text,
                                R.id.list_textview, options);
                        itemList.setAdapter(itemAdapter);
                        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView,
                                                    View view, int i, long l) {
                                switch (i) {
                                    case 0:
                                        editExercise(adapterView
                                                .getItemAtPosition(i)
                                                .toString());
                                        dialog2.dismiss();
                                        break;
                                    case 1:
                                        deleteExercise(adapterView
                                                .getItemAtPosition(i)
                                                .toString());
                                        dialog2.dismiss();
                                        break;
                                    case 2:
                                        dialog2.dismiss();
                                        break;
                                }
                            }
                        });
                        return false;
                    }
                });
        createBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View p1) {
                // FEATURE for later, figure out what to pass here for creating
                // a workout log based on the current program

                Intent intent = new Intent(Programs.this, WorkoutLog.class);
                intent.putExtra("programName", programNameSpin
                        .getSelectedItem().toString());
                startActivity(intent);
            }
        });
        saveBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View p1) {

                String newDesc = programDescriptionEdit.getText().toString();
                programDescription.setText(newDesc);
                programDescriptionEdit.setText(newDesc);
            }
        });
        discardBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

    }

    private void editExercise(final String exercise) {

    }

    private void deleteExercise(final String exercise) {

    }

    private void loadDataForProgram(final String program) {
        int index = programNameList.indexOf(program);
        programDescription
                .setText(programDescriptionList.get(index));
        programDescriptionEdit.setText(programDescriptionList.get(index));
        try {
            String[] exerciseNameArray = exerciseNameList.get(index).split("-");
            exerciseNameList2.clear();
            Collections.addAll(exerciseNameList2, exerciseNameArray);
        } catch (Exception e) {
            e.printStackTrace();
            exerciseNameList2.add(index,
                    getResources().getString(R.string.no_exercises));
        }
        adapterList.notifyDataSetChanged();
    }

    /**
     * This gets all programs from the db and loads it into the layout
     */
    private void getAllProgramData() {
        // access the db and pull out all program names
        dbAdapter.open();
        Cursor cursor = dbAdapter.getAllPrograms();
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            // get program name, description
            programNameList.add(cursor.getString(1));
            programDescriptionList.add(cursor.getString(2));
            exerciseNameList.add(cursor.getString(3));
            if (exerciseNameList.get(i).equals("")
                    || exerciseNameList.get(i) == null) {
                exerciseNameList.remove(i);
                exerciseNameList.add(i,
                        getResources().getString(R.string.no_exercises) + "-");
            }
            cursor.moveToNext();
        }
        cursor.close();
        dbAdapter.close();
    }

    private void addNewProgram() {
        Intent intent = new Intent(Programs.this, CreateProgram.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.programs_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //menu.findItem(R.id.action_new).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            //case R.id.action_new:
            //	addNewProgram();
            //	return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
/**
 * From {@link com.r3b00tz.trainingtoolkit.activities.WorkoutLog}
 * <p>
 * private void setupProgram(String programName) {
 * if (programNameList.contains(programName)) {
 * int index = programNameList.indexOf(programName);
 * String[] toParse = programExerciseList.get(index)
 * .split("-");
 * <p>
 * Collections.addAll(exercisesUsedList, toParse);
 * initAllViews();
 * } else {
 * initBlankWorkoutLog();
 * }
 * }
 *
 * @SuppressWarnings("unused") private void loadProgramSelected(String programName) {
 * exercisesUsedList.clear();
 * int index = programNameList.indexOf(programName);
 * String name = programNameList.get(index);
 * String[] exercises = programExerciseList.get(index)
 * .split("-");
 * <p>
 * Collections.addAll(exercisesUsedList, exercises);
 * adapter.notifyDataSetChanged();
 * }
 * <p>
 * private void getExerciseProgramData() {
 * programNameList.add("None");
 * programExerciseList.add("None");
 * dbAdapter.open();
 * cursor = dbAdapter.getAllPrograms();
 * cursor.moveToFirst();
 * Cursor cursor2 = dbAdapter.getAllExercises();
 * cursor2.moveToFirst();
 * for (int i = 0; i < cursor.getCount(); i++) {
 * programNameList.add(cursor.getString(1));
 * programExerciseList.add(cursor.getString(3));
 * cursor.moveToNext();
 * }
 * for (int i = 0; i < cursor2.getCount(); i++) {
 * exercises.add(cursor2.getString(1));
 * cursor2.moveToNext();
 * }
 * cursor.close();
 * cursor2.close();
 * dbAdapter.close();
 * }
 **/