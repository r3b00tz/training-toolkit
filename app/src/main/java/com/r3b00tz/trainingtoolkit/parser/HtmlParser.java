package com.r3b00tz.trainingtoolkit.parser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class HtmlParser {
    private String urlToParse;
    private StringBuilder pageHTMLString = new StringBuilder();

    /**
     * Constructor. Takes in the string url you want to parse and if you want
     * debug logging to be turned on or off.
     *
     * @param urlToParse
     */
    public HtmlParser(String urlToParse) {
        this.urlToParse = urlToParse;
    }

    public String cleanupContent(String pageContents) {
        pageContents = pageContents.replaceAll("<p>", "");
        pageContents = pageContents.replaceAll("</p>", "");
        pageContents = pageContents.replaceAll("<br />", "");
        pageContents = pageContents.replaceAll("<strong>", "");
        pageContents = pageContents.replaceAll("</strong>", "");
        pageContents = pageContents.replaceAll("<a href=\"(.*)\">", "");
        pageContents = pageContents.replaceAll("</a>", "");
        return pageContents;
    }

    /**
     * Allows you to pull string contents out by looking for a tag and it's css
     * class
     *
     * @param tag      - string tag value, ex 'p', 'a', 'span'
     * @param cssClass - string css class value
     */
    public String getContentsByTagAndClass(final String tag,
                                           final String cssClass) {
        String startTag = "<" + tag;
        String contentStartTag = "<p>";
        String contentEndTag = "</p>";
        String desiredContent;
        if (pageHTMLString.length() > 0 || pageHTMLString != null) {
            try {
                int firstStartTagIndex = pageHTMLString.indexOf(cssClass);
                //Log.i("TEST", "firstStartTagIndex : "+firstStartTagIndex);
                int firstEndTagIndex = pageHTMLString.indexOf(startTag,
                        firstStartTagIndex);

                pageHTMLString.substring(firstStartTagIndex, firstEndTagIndex);

                int nextStartIndex = pageHTMLString.indexOf(cssClass,
                        firstEndTagIndex);
                int nextEndIndex = pageHTMLString.indexOf(startTag, nextStartIndex);

                String test2 = pageHTMLString.substring(nextStartIndex,
                        nextEndIndex);
                //Log.i("TEST1 html ", test2);
                int contentStart = test2.indexOf(contentStartTag);
                int contentEnd;
                if (test2.contains("Rest Day")) {
                    contentEnd = test2.indexOf("</strong>");
                } else {
                    contentEnd = test2.indexOf("<p>Post");
                }

                desiredContent = test2.substring(contentStart, contentEnd);
            } catch (Exception e) {
                int firstStartTagIndex = pageHTMLString.indexOf(cssClass);
                int firstEndTagIndex = pageHTMLString.indexOf(startTag,
                        firstStartTagIndex);
                //Log.i("TEST", "page String: "+pageHTMLString);
                pageHTMLString.substring(firstStartTagIndex, firstEndTagIndex);

                int nextStartIndex = pageHTMLString.indexOf(cssClass,
                        firstEndTagIndex);
                int nextEndIndex = pageHTMLString.indexOf(startTag, nextStartIndex);

                String test2 = pageHTMLString.substring(nextStartIndex,
                        nextEndIndex);
                //Log.i("TEST html ", test2);
                int contentStart = test2.indexOf(contentStartTag);
                int contentEnd = test2.indexOf(contentEndTag);
                desiredContent = test2.substring(contentStart, contentEnd);
            }
        } else {
            return null;// our pageHTMLString is empty and we can't do anything
        }
        return desiredContent;
    }

    public StringBuilder openURLOnThread() {
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    URL url = new URL(urlToParse);
                    URLConnection yc = url.openConnection();
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(yc.getInputStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        pageHTMLString.append(inputLine);
                        pageHTMLString.append("\n");
                    }
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        while (t.isAlive()) {
            // do nothing
        }
        return pageHTMLString;
    }
}
