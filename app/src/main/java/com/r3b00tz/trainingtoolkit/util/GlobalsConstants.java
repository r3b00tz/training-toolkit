package com.r3b00tz.trainingtoolkit.util;

/**
 * Created by Matt on 1/28/2017.
 */

public class GlobalsConstants {

    //Constants
    public static final String IMPERIAL = "Imperial";
    public static final String METRIC = "Metric";
    public static final double KG_TO_POUND_MULTIPLIER = 2.20462;
    public static final double CM_TO_INCHES_MULTIPLIER = 0.393701;

    //app
    public static boolean UsingImperial = true;

    //activity
}
