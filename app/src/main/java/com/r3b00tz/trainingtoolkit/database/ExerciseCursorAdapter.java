package com.r3b00tz.trainingtoolkit.database;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;

/**
 * Created by Matt on 2/14/2017.
 */

public class ExerciseCursorAdapter extends CursorAdapter {

    public class ViewHolder {
        TextView exerciseName;
    }

    public ExerciseCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    public ExerciseCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.exercise_name_list_item,
                parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.exerciseName = (TextView) view.findViewById(R.id.textListItem);

        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.exerciseName.setText(cursor.getString(1));

    }
}
