package com.r3b00tz.trainingtoolkit.database;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by Matt on 2/14/2017.
 */

public class ExerciseCursorLoader extends SimpleCursorLoader {

    public DBAdapter dbAdapter;

    public ExerciseCursorLoader(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {
        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();
        Cursor cursor = dbAdapter.getAllExercises();//TODO fix? dbAdapter.getAllExercisesAlphabetical();

        return cursor;
    }

    public void closeDB() {
        dbAdapter.close();
    }
}
