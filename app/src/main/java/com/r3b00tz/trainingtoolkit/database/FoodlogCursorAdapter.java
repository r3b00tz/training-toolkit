package com.r3b00tz.trainingtoolkit.database;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.FoodLog;
import com.r3b00tz.trainingtoolkit.adapter.RecyclerViewCursorAdapter;

/**
 * Created by Matt on 2/21/2017.
 */

public class FoodlogCursorAdapter extends RecyclerViewCursorAdapter<FoodlogCursorAdapter.FoodlogViewHolder> {

    public FoodlogCursorAdapter(Cursor cursor) {
        super(cursor);
    }

    @Override
    public FoodlogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.existing_food_log_item, parent, false);

        return new FoodlogViewHolder(v);
    }

    @Override
    protected void onBindViewHolder(FoodlogViewHolder holder, Cursor cursor) {

        final String date = cursor.getString(1);
        holder.textFoodLogDate.setText(date);

        holder.foodLogItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FoodLog.class);
                intent.putExtra("selectedDate", date);
                v.getContext().startActivity(intent);
            }
        });

        holder.foodLogItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Context context = v.getContext();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(context.getResources().getString(R.string.delete_title));
                alertDialogBuilder
                        .setMessage(context.getResources().getString(R.string.delete_message))
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                DBAdapter dbAdapter = new DBAdapter(context);
                                dbAdapter.open();
                                dbAdapter.deleteFoodLogByDate(date);
                                dbAdapter.close();

                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    static class FoodlogViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CardView foodLogItem;
        TextView textFoodLogDate;

        FoodlogViewHolder(View v) {
            super(v);
            foodLogItem = (CardView) v.findViewById(R.id.foodLogItem);
            textFoodLogDate = (TextView) v.findViewById(R.id.textFoodLogDate);
        }
    }
}
