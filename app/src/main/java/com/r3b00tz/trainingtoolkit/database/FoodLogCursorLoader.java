package com.r3b00tz.trainingtoolkit.database;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by Matt on 2/21/2017.
 */

public class FoodLogCursorLoader extends SimpleCursorLoader {

    public DBAdapter dbAdapter;

    public FoodLogCursorLoader(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {
        dbAdapter = new DBAdapter(getContext());
        dbAdapter.open();
        Cursor cursor = dbAdapter.getAllFoodLogs();

        return cursor;
    }

    public void closeDB() {
        dbAdapter.close();
    }
}
