package com.r3b00tz.trainingtoolkit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.models.Exercise;
import com.r3b00tz.trainingtoolkit.models.StatsModel;
import com.r3b00tz.trainingtoolkit.models.WorkoutLogModel;
import com.r3b00tz.trainingtoolkit.util.GlobalsConstants;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Matt on 1/21/2017.
 */

public class DBAdapter {
    private static final String DATABASE_NAME = "JournalDB.db";
    private static final int DATABASE_VERSION = 1;
    private static final String ROWID = "_id";

    private static final String DATABASE_TABLE_PREFERENCES = "App_Preferences";
    private static final String PREFERENCES_EXERCISES_REPMAX = "Preferences_Exercises_RepMax";
    private static final String PREFERENCES_METRIC_IMPERIAL = "Preferences_metric_imperial";

    private static final String DATABASE_TABLE_PROGRAMS = "Programs";
    private static final String PROGRAM_NAME = "ProgramName";
    private static final String PROGRAM_DESCRIPTION = "ProgramDescription";
    private static final String PROGRAM_EXERCISES = "ProgramExercises";
    private static final String PROGRAM_WORK_SCHEME = "ProgramWorkScheme";

    private static final String DATABASE_TABLE_CUSTOMFOOD = "Custom_Food";
    private static final String CUSTOMFOOD_NAME = "CustomFood_Name";
    private static final String CUSTOMFOOD_SERVING_SIZE = "CustomFood_Serving_Size";
    private static final String CUSTOMFOOD_CALORIES = "CustomFood_Calories";
    private static final String CUSTOMFOOD_PROTEIN = "CustomFood_Protein";
    private static final String CUSTOMFOOD_CARBS = "CustomFood_Carbs";
    private static final String CUSTOMFOOD_FAT = "CustomFood_Fat";

    private static final String DATABASE_TABLE_EXERCISES = "Exercises";
    private static final String EXERCISE_NAME = "ExerciseName";
    private static final String EXERCISE_DESCRIPTION = "ExerciseDescription";
    private static final String EXERCISE_1RM = "Exercise1RM";
    private static final String EXERCISE_3RM = "Exercise3RM";
    private static final String EXERCISE_5RM = "Exercise5RM";
    private static final String EXERCISE_10RM = "Exercise10RM";
    private static final String EXERCISE_PR_VALUE = "prValue";
    private static final String EXERCISE_PR_GOAL = "prGoal";
    private static final String EXERCISE_LAST_WEIGHT_USED = "lastWeightUsed";
    private static final String EXERCISE_LAST_SET_SCHEME = "lastSetScheme";
    private static final String EXERCISE_LAST_REP_SCHEME = "lastRepScheme";
    private static final String EXERCISE_LAST_ROUND_COMPLETE = "lastRoundComplete";
    private static final String EXERCISE_LAST_PERCENTAGE_RESISTANCE = "lastPercentageResistance";
    private static final String EXERCISE_LAST_TIME_LIMIT = "lastTimeLimit";

    private static final String DATABASE_TABLE_FOODLOG = "Food_Log";
    private static final String FOOD_LOG_DATE = "FoodLog_Date";
    private static final String FOOD_LOG_FOODNAME = "FoodLog_Foodname";
    private static final String FOOD_LOG_CALORIES = "FoodLog_Calories";
    private static final String FOOD_LOG_PROTEIN = "FoodLog_Protein";
    private static final String FOOD_LOG_CARBS = "FoodLog_Carbs";
    private static final String FOOD_LOG_FAT = "FoodLog_Fat";
    private static final String FOOD_LOG_SERVING_SIZE = "FoodLog_Serving_Size";

    private static final String DATABASE_TABLE_GOAL = "Goals";
    private static final String GOAL_NAME = "Goal_Name";
    private static final String GOAL_START_DATE = "Goal_Start_Date";
    private static final String GOAL_END_DATE = "Goal_End_Date";
    private static final String GOAL_CURRENT_DATE = "Goal_Current_Date";
    private static final String GOAL_DESCRIPTION = "Goal_Description";
    private static final String GOAL_WEIGHT = "Goal_Weight";
    private static final String GOAL_BFPERCENT = "Goal_BFPercent";

    private static final String GOAL_MAX_EXERCISE = "Goal_Max_Exercise";
    private static final String GOAL_REP_MAX = "Goal_Rep_Max";

    private static final String DATABASE_TABLE_STATS = "StatsModel";
    private static final String STATS_DATE = "Stats_Date";
    private static final String STATS_BFPERCENT = "Stats_BFPercent";
    private static final String STATS_HEIGHT = "Stats_Height";
    private static final String STATS_WEIGHT = "Stats_Weight";
    private static final String STATS_DAILY_CALS = "Stats_Daily_Cals";
    private static final String STATS_DAILY_CARBS = "Stats_Daily_Carbs";
    private static final String STATS_DAILY_PROTEIN = "Stats_Daily_Protein";
    private static final String STATS_DAILY_FAT = "Stats_Daily_Fat";

    private static final String DATABASE_TABLE_USER = "User_Data";
    private static final String USERS_CURRENT_DATE = "Users_Current_Date";
    private static final String USERS_BFPERCENT = "Users_BFPercent";
    private static final String USERS_WEIGHT = "Users_Weight";
    private static final String USERS_LAST_WORKOUT_DATE = "Users_Last_Workout_Date";
    private static final String USERS_LAST_FOOD_LOG_DATE = "Users_Last_FoodLog_Date";

    private static final String DATABASE_TABLE_WORKOUTLOG = "Workout_Log";
    private static final String WORKOUT_LOG_DATE = "WorkoutLog_Date";
    private static final String WORKOUT_LOG_NAME = "WorkoutLog_Name";
    private static final String WORKOUT_LOG_EXERCISE_NAME = "WorkoutLog_Exercise_Name";
    private static final String WORKOUT_LOG_EXERCISE_WEIGHT = "WorkoutLog_Exercise_Weight";
    private static final String WORKOUT_LOG_EXERCISE_REPS = "WorkoutLog_Exercise_Reps";
    private static final String WORKOUT_LOG_EXERCISE_SETS = "WorkoutLog_Exercise_Sets";
    private static final String WORKOUT_LOG_EXERCISE_LAST_ROUND_COMPLETE = "lastRoundComplete";
    private static final String WORKOUT_LOG_EXERCISE_LAST_PERCENTAGE_RESISTANCE = "lastPercentageResistance";
    private static final String WORKOUT_LOG_EXERCISE_LAST_TIME_LIMIT = "lastTimeLimit";
    private static final String WORKOUT_LOG_EXERCISE_USER_COMMENTS = "WorkoutLog_Exercise_User_comments";

    private static final String TAG = "DBAdapter";

    private DatabaseHelper DBHelper;
    private Cursor cursor;
    private SQLiteDatabase db;
    private Context context;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    // ---opens the database---
    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    // ---closes the database---
    public void close() {
        DBHelper.close();
    }

    public void makeAllTables() {
        makePreferencesTable();
        makeProgramTable();
        makeExerciseTable();
        makeWorkoutLogTable();
        makeFoodLogTable();
        makeStatTable();
        makeGoalsTable();
        makeUsersTable();
        makeCustomFoodTable();
    }

    public void loadInitialData() {
        String[] fieldData = context.getResources().getStringArray(
                R.array.exercise_names);
        String[] fieldData2 = new String[6];
        fieldData2[0] = context.getResources().getString(R.string.program1);
        fieldData2[1] = context.getResources().getString(R.string.program2);
        fieldData2[2] = context.getResources().getString(R.string.program3);
        fieldData2[3] = context.getResources().getString(R.string.program4);
        fieldData2[4] = context.getResources().getString(R.string.program5);
        fieldData2[5] = context.getResources().getString(R.string.program6);
        String[] fieldData3 = context.getResources().getStringArray(
                R.array.program_descriptions);

        Arrays.sort(fieldData);

        insertPreferences();
        insertExercises(fieldData);
        insertInitialCustomFood();
        insertProgramNames(fieldData2, fieldData3);
        insertInitialStats();
    }

    private void insertInitialCustomFood() {
        String name = context.getResources().getString(R.string.water);
        String servingSize = context.getResources().getString(
                R.string.servesize);
        String calories = context.getResources().getString(R.string.zero);
        String protein = context.getResources().getString(R.string.zero);
        String carbs = context.getResources().getString(R.string.zero);
        String fat = context.getResources().getString(R.string.zero);
        createCustomFood(name, servingSize, calories, protein, carbs, fat);
    }

    private void insertInitialStats() {
        createStat("", "", "", "", "", "", "", "");
    }

    private void insertPreferences() {
        String defaultExercises = "Bench Press-Squat-Deadlift";
        String metricImperial = GlobalsConstants.IMPERIAL;
        createPreference(defaultExercises, metricImperial);
    }

    private void insertExercises(String[] data) {
        for (String aData : data) {
            createExercise(aData, "empty", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0");
        }
    }

    private void insertProgramNames(String[] data, String[] data2) {
        String[] defaultExercises = {"Bench-Squat-Deadlift", "",
                "Bench-Deadlift", "Bench-Squat-Deadlift", "Bench", "Squat"};
        for (int i = 0; i < data.length; i++) {
            createProgram(data[i], data2[i], defaultExercises[i], "");
        }
    }

    private void makePreferencesTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_PREFERENCES + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PREFERENCES_EXERCISES_REPMAX + " TEXT, " + PREFERENCES_METRIC_IMPERIAL + " TEXT);");
    }

    private void makeCustomFoodTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_CUSTOMFOOD + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CUSTOMFOOD_NAME + " TEXT, " + CUSTOMFOOD_SERVING_SIZE + " TEXT, " + CUSTOMFOOD_CALORIES + " TEXT, " + CUSTOMFOOD_PROTEIN + " TEXT, " + CUSTOMFOOD_CARBS + " TEXT, " + CUSTOMFOOD_FAT + " TEXT);");
    }

    private void makeUsersTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_USER + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USERS_CURRENT_DATE + " TEXT, " + USERS_BFPERCENT + " TEXT, " + USERS_WEIGHT + " TEXT, " + USERS_LAST_WORKOUT_DATE + " TEXT, " + USERS_LAST_FOOD_LOG_DATE + " TEXT);");
    }

    private void makeGoalsTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_GOAL + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + GOAL_NAME + " TEXT, " + GOAL_START_DATE + " TEXT, " + GOAL_END_DATE + " TEXT, " + GOAL_CURRENT_DATE + " TEXT, " + GOAL_DESCRIPTION + " TEXT, " + GOAL_WEIGHT + " TEXT, " + GOAL_BFPERCENT + " TEXT, " + GOAL_MAX_EXERCISE + " TEXT, " + GOAL_REP_MAX + " TEXT);");
    }

    private void makeStatTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_STATS + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + STATS_DATE + " TEXT, " + STATS_BFPERCENT + " TEXT, " + STATS_HEIGHT + " TEXT, " + STATS_WEIGHT + " TEXT, " + STATS_DAILY_CALS + " TEXT, " + STATS_DAILY_CARBS + " TEXT, " + STATS_DAILY_PROTEIN + " TEXT, " + STATS_DAILY_FAT + " TEXT);");
    }

    private void makeFoodLogTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_FOODLOG + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + FOOD_LOG_DATE + " TEXT, " + FOOD_LOG_FOODNAME + " TEXT, " + FOOD_LOG_CALORIES + " TEXT, " + FOOD_LOG_PROTEIN + " TEXT, " + FOOD_LOG_CARBS + " TEXT, " + FOOD_LOG_FAT + " TEXT, " + FOOD_LOG_SERVING_SIZE + " TEXT);");
    }

    private void makeWorkoutLogTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_WORKOUTLOG + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + WORKOUT_LOG_DATE + " TEXT, " + WORKOUT_LOG_NAME + " TEXT, " + WORKOUT_LOG_EXERCISE_NAME + " TEXT, " + WORKOUT_LOG_EXERCISE_WEIGHT + " TEXT, " + WORKOUT_LOG_EXERCISE_REPS + " TEXT, " + WORKOUT_LOG_EXERCISE_SETS + " TEXT, " + WORKOUT_LOG_EXERCISE_LAST_PERCENTAGE_RESISTANCE + " TEXT, " + WORKOUT_LOG_EXERCISE_LAST_TIME_LIMIT + " TEXT, " + WORKOUT_LOG_EXERCISE_LAST_ROUND_COMPLETE + " TEXT, " + WORKOUT_LOG_EXERCISE_USER_COMMENTS + " TEXT);");
    }

    private void makeExerciseTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_EXERCISES + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EXERCISE_NAME + " TEXT, " + EXERCISE_DESCRIPTION + " TEXT, " + EXERCISE_1RM + " TEXT, " + EXERCISE_3RM + " TEXT, " + EXERCISE_5RM + " TEXT, " + EXERCISE_10RM + " TEXT, " + EXERCISE_PR_VALUE + " TEXT, " + EXERCISE_PR_GOAL + " TEXT, " + EXERCISE_LAST_WEIGHT_USED + " TEXT, " + EXERCISE_LAST_SET_SCHEME + " TEXT, " + EXERCISE_LAST_REP_SCHEME + " TEXT, " + EXERCISE_LAST_ROUND_COMPLETE + " TEXT, " + EXERCISE_LAST_PERCENTAGE_RESISTANCE + " TEXT, " + EXERCISE_LAST_TIME_LIMIT + " TEXT);");
    }

    private void makeProgramTable() {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_PROGRAMS + "(" + ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PROGRAM_NAME + " TEXT, " + PROGRAM_DESCRIPTION + " TEXT, " + PROGRAM_EXERCISES + " TEXT, " + PROGRAM_WORK_SCHEME + " TEXT);");
    }

    // App_Preferences methods
    public ArrayList<String> getPreference(long rowId) throws SQLException {
        cursor = db.query(DATABASE_TABLE_PREFERENCES, new String[]{ROWID,
                        PREFERENCES_EXERCISES_REPMAX, PREFERENCES_METRIC_IMPERIAL}, null, null,
                null, null, null, null);
        cursor.moveToFirst();

        // this cycles down to the chosen row, as we're already at the first
        // index we take the row id and do a < comparison
        int j = 1;// cursors start at row 1, once this is equal to the rowID
        // chosen, it moves down to the for loop to pull the fields
        while (j < rowId) {
            cursor.moveToNext();
            j++;
        }

        ArrayList<String> result = new ArrayList<>();
        if (cursor.getCount() != 0 && cursor.getString(1) != null) {
            result.add(cursor.getString(0));
            result.add(cursor.getString(1));
            result.add(cursor.getString(2));
        } else {
            // "EMPTY RESULT OR REMAINING COLUMNS NULL");
            return result;
        }
        return result;
    }

    public boolean updatePreference(String exercises, String metricImperial) {
        ContentValues args = new ContentValues();
        args.put(PREFERENCES_EXERCISES_REPMAX, exercises);
        args.put(PREFERENCES_METRIC_IMPERIAL, metricImperial);
        return this.db.update(DATABASE_TABLE_PREFERENCES, args,
                ROWID + "=" + 1, null) > 0;
    }

    private long createPreference(String exercises, String metricImperial) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(PREFERENCES_EXERCISES_REPMAX, exercises);
        initialValues.put(PREFERENCES_METRIC_IMPERIAL, metricImperial);
        return this.db.insert(DATABASE_TABLE_PREFERENCES, null, initialValues);
    }

    // Program methods
    public Cursor getAllPrograms() {
        return this.db.query(DATABASE_TABLE_PROGRAMS, new String[]{ROWID,
                PROGRAM_NAME, PROGRAM_DESCRIPTION, PROGRAM_EXERCISES,
                PROGRAM_WORK_SCHEME}, null, null, null, null, null);
    }

    public long createProgram(String name, String description,
                              String exercises, String workScheme) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(PROGRAM_NAME, name);
        initialValues.put(PROGRAM_DESCRIPTION, description);
        initialValues.put(PROGRAM_EXERCISES, exercises);
        initialValues.put(PROGRAM_WORK_SCHEME, workScheme);
        return this.db.insert(DATABASE_TABLE_PROGRAMS, null, initialValues);
    }

    // Custom Food Methods
    public long createCustomFood(String name, String servingSize,
                                 String calories, String protein, String carbs, String fat) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(CUSTOMFOOD_NAME, name);
        initialValues.put(CUSTOMFOOD_SERVING_SIZE, servingSize);
        initialValues.put(CUSTOMFOOD_CALORIES, calories);
        initialValues.put(CUSTOMFOOD_PROTEIN, protein);
        initialValues.put(CUSTOMFOOD_CARBS, carbs);
        initialValues.put(CUSTOMFOOD_FAT, fat);
        return this.db.insert(DATABASE_TABLE_CUSTOMFOOD, null, initialValues);
    }

    public boolean deleteCustomFood(long rowId) {
        return this.db.delete(DATABASE_TABLE_CUSTOMFOOD, ROWID + "=" + rowId,
                null) > 0;
    }

    public Cursor getAllCustomFood() {
        return this.db.query(DATABASE_TABLE_CUSTOMFOOD, new String[]{ROWID,
                        CUSTOMFOOD_NAME, CUSTOMFOOD_SERVING_SIZE, CUSTOMFOOD_CALORIES,
                        CUSTOMFOOD_PROTEIN, CUSTOMFOOD_CARBS, CUSTOMFOOD_FAT}, null,
                null, null, null, null);
    }

    public Cursor getCustomFoodByName(String name) {
        return db.rawQuery("SELECT * FROM " + DATABASE_TABLE_CUSTOMFOOD
                + " WHERE " + CUSTOMFOOD_NAME + " = '" + name + "'", null);
    }

    public boolean updateCustomFood(long rowId, String name,
                                    String servingSize, String calories, String protein, String carbs,
                                    String fat) {
        ContentValues args = new ContentValues();
        args.put(CUSTOMFOOD_NAME, name);
        args.put(CUSTOMFOOD_SERVING_SIZE, servingSize);
        args.put(CUSTOMFOOD_CALORIES, calories);
        args.put(CUSTOMFOOD_PROTEIN, protein);
        args.put(CUSTOMFOOD_CARBS, carbs);
        args.put(CUSTOMFOOD_FAT, fat);
        return this.db.update(DATABASE_TABLE_CUSTOMFOOD, args, ROWID + "="
                + rowId, null) > 0;
    }

    // Exercise Methods
    public boolean doesExerciseExist(String name) throws SQLException {
        try {
            cursor = db.query(DATABASE_TABLE_EXERCISES, new String[]{
                    ROWID, EXERCISE_NAME, EXERCISE_DESCRIPTION, EXERCISE_1RM, EXERCISE_3RM, EXERCISE_5RM, EXERCISE_10RM, EXERCISE_PR_VALUE, EXERCISE_PR_GOAL, EXERCISE_LAST_WEIGHT_USED, EXERCISE_LAST_SET_SCHEME, EXERCISE_LAST_REP_SCHEME, EXERCISE_LAST_ROUND_COMPLETE, EXERCISE_LAST_PERCENTAGE_RESISTANCE, EXERCISE_LAST_TIME_LIMIT}, null, null, null, null, null, null);

            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                if (cursor.getString(1).equals(name)) {
                    return true;
                }
                cursor.moveToNext();
            }
            return false;
        } catch (Exception e) { //nothing
        }
        return false;
    }

    public long createExercise(String name, String description, String exercise1RM, String exercise3RM, String exercise5RM, String exercise10RM, String prValue, String prGoal, String lastWeightUsed, String lastSetScheme, String lastRepScheme, String lastRoundComplete, String lastPercentageResistance, String lastTimeLimit) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(EXERCISE_NAME, name);
        initialValues.put(EXERCISE_DESCRIPTION, description);
        initialValues.put(EXERCISE_1RM, exercise1RM);
        initialValues.put(EXERCISE_3RM, exercise3RM);
        initialValues.put(EXERCISE_5RM, exercise5RM);
        initialValues.put(EXERCISE_10RM, exercise10RM);
        initialValues.put(EXERCISE_PR_VALUE, prValue);
        initialValues.put(EXERCISE_PR_GOAL, prGoal);
        initialValues.put(EXERCISE_LAST_WEIGHT_USED, lastWeightUsed);
        initialValues.put(EXERCISE_LAST_SET_SCHEME, lastSetScheme);
        initialValues.put(EXERCISE_LAST_REP_SCHEME, lastRepScheme);
        initialValues.put(EXERCISE_LAST_ROUND_COMPLETE, lastRoundComplete);
        initialValues.put(EXERCISE_LAST_PERCENTAGE_RESISTANCE, lastPercentageResistance);
        initialValues.put(EXERCISE_LAST_TIME_LIMIT, lastTimeLimit);
        return this.db.insert(DATABASE_TABLE_EXERCISES, null, initialValues);
    }

    public boolean deleteExerciseByName(String name) {
        return this.db.delete(DATABASE_TABLE_EXERCISES, EXERCISE_NAME + "=?", new String[]{name}) > 0;
    }

    public Exercise getExerciseByName(String name) {
        Cursor c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_EXERCISES
                + " WHERE " + EXERCISE_NAME + " = '" + name + "'", null);
        c.moveToFirst();
        if (c.getCount() > 0) {
            Exercise exercise = new Exercise();
            exercise.setName(c.getString(1));
            exercise.setDescription(c.getString(2));
            exercise.setOneRepMax(c.getInt(3));
            exercise.setThreeRepMax(c.getInt(4));
            exercise.setFiveRepMax(c.getInt(5));
            exercise.setTenRepMax(c.getInt(6));
            exercise.setPrValue(c.getInt(7));
            exercise.setPrGoal(c.getInt(8));
            exercise.setLastWeightUsed(c.getInt(9));
            exercise.setLastSetScheme(c.getInt(10));
            exercise.setLastRepScheme(c.getInt(11));
            exercise.setLastRoundComplete(c.getInt(12));
            exercise.setLastPercentageResistance(c.getDouble(13));
            exercise.setLastTimeLimit(c.getLong(14));
            c.close();
            return exercise;
        }
        c.close();
        return null;
    }

    public Cursor getAllExercises() {
        return this.db.query(DATABASE_TABLE_EXERCISES, new String[]{ROWID, EXERCISE_NAME, EXERCISE_DESCRIPTION, EXERCISE_1RM, EXERCISE_3RM, EXERCISE_5RM, EXERCISE_10RM, EXERCISE_PR_VALUE, EXERCISE_PR_GOAL, EXERCISE_LAST_WEIGHT_USED, EXERCISE_LAST_SET_SCHEME, EXERCISE_LAST_REP_SCHEME, EXERCISE_LAST_ROUND_COMPLETE, EXERCISE_LAST_PERCENTAGE_RESISTANCE, EXERCISE_LAST_TIME_LIMIT}, null, null, null, null, null);
    }

    public ArrayList<String> getAllExerciseNames() {
        ArrayList<String> exerciseList = new ArrayList<>();

        Cursor cursor = this.db.query(DATABASE_TABLE_EXERCISES, new String[]{ROWID, EXERCISE_NAME, EXERCISE_DESCRIPTION, EXERCISE_1RM, EXERCISE_3RM, EXERCISE_5RM, EXERCISE_10RM, EXERCISE_PR_VALUE, EXERCISE_PR_GOAL,
                EXERCISE_LAST_WEIGHT_USED, EXERCISE_LAST_SET_SCHEME, EXERCISE_LAST_REP_SCHEME, EXERCISE_LAST_ROUND_COMPLETE, EXERCISE_LAST_PERCENTAGE_RESISTANCE, EXERCISE_LAST_TIME_LIMIT}, null, null, null, null, null);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                exerciseList.add(cursor.getString(1));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return exerciseList;
    }

    public boolean updateExerciseByName(String name, String description,
                                        String exercise1RM, String exercise3RM, String exercise5RM, String exercise10RM, String prValue, String prGoal, String lastWeightUsed, String lastSetScheme, String lastRepScheme, String lastRoundComplete, String lastPercentageResistance, String lastTimeLimit) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(EXERCISE_NAME, name);
        initialValues.put(EXERCISE_DESCRIPTION, description);
        initialValues.put(EXERCISE_1RM, exercise1RM);
        initialValues.put(EXERCISE_3RM, exercise3RM);
        initialValues.put(EXERCISE_5RM, exercise5RM);
        initialValues.put(EXERCISE_10RM, exercise10RM);
        initialValues.put(EXERCISE_PR_VALUE, prValue);
        initialValues.put(EXERCISE_PR_GOAL, prGoal);
        initialValues.put(EXERCISE_LAST_WEIGHT_USED, lastWeightUsed);
        initialValues.put(EXERCISE_LAST_SET_SCHEME, lastSetScheme);
        initialValues.put(EXERCISE_LAST_REP_SCHEME, lastRepScheme);
        initialValues.put(EXERCISE_LAST_ROUND_COMPLETE, lastRoundComplete);
        initialValues.put(EXERCISE_LAST_PERCENTAGE_RESISTANCE, lastPercentageResistance);
        initialValues.put(EXERCISE_LAST_TIME_LIMIT, lastTimeLimit);

        return this.db.update(DATABASE_TABLE_EXERCISES, initialValues, EXERCISE_NAME + "=?", new String[]{name}) > 0;
    }

    // foodlog methods
    public long createFoodLog(String date, String foodName, String calories,
                              String protein, String carbs, String fat, String servSize) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(FOOD_LOG_DATE, date);
        initialValues.put(FOOD_LOG_FOODNAME, foodName);
        initialValues.put(FOOD_LOG_CALORIES, calories);
        initialValues.put(FOOD_LOG_PROTEIN, protein);
        initialValues.put(FOOD_LOG_CARBS, carbs);
        initialValues.put(FOOD_LOG_FAT, fat);
        initialValues.put(FOOD_LOG_SERVING_SIZE, servSize);
        return this.db.insert(DATABASE_TABLE_FOODLOG, null, initialValues);
    }

    public boolean doesFoodLogExist(String date) {
        try {
            cursor = db.query(DATABASE_TABLE_FOODLOG, new String[]{
                    ROWID, FOOD_LOG_DATE, FOOD_LOG_FOODNAME, FOOD_LOG_CALORIES,
                    FOOD_LOG_PROTEIN, FOOD_LOG_CARBS, FOOD_LOG_FAT, FOOD_LOG_SERVING_SIZE}, null, null, null, null, null, null);

            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                if (cursor.getString(1).equals(date)) {
                    return true;
                }
                cursor.moveToNext();
            }
            return false;
        } catch (Exception e) { //nothing
        }
        return false;
    }

    public boolean deleteFoodLogByDate(String date) {
        return this.db.delete(DATABASE_TABLE_FOODLOG, FOOD_LOG_DATE + "=?", new String[]{date}) > 0;
    }

    public Cursor getAllFoodLogs() {
        return this.db.query(DATABASE_TABLE_FOODLOG, new String[]{ROWID,
                FOOD_LOG_DATE, FOOD_LOG_FOODNAME, FOOD_LOG_CALORIES,
                FOOD_LOG_PROTEIN, FOOD_LOG_CARBS, FOOD_LOG_FAT, FOOD_LOG_SERVING_SIZE}, null, null, null, null, null);
    }

    public Cursor selectFoodLogByDate(String date) {
        return db.rawQuery("SELECT * FROM " + DATABASE_TABLE_FOODLOG
                + " WHERE " + FOOD_LOG_DATE + " = '" + date + "'", null);
    }

    public boolean updateExistingFoodLog(long rowId, String date,
                                         String foodName, String calories, String protein, String carbs,
                                         String fat, String servSize) {
        ContentValues args = new ContentValues();
        args.put(FOOD_LOG_DATE, date);
        args.put(FOOD_LOG_FOODNAME, foodName);
        args.put(FOOD_LOG_CALORIES, calories);
        args.put(FOOD_LOG_PROTEIN, protein);
        args.put(FOOD_LOG_CARBS, carbs);
        args.put(FOOD_LOG_FAT, fat);
        args.put(FOOD_LOG_SERVING_SIZE, servSize);
        return this.db.update(DATABASE_TABLE_FOODLOG, args,
                ROWID + "=?", new String[]{String.valueOf(rowId)}) > 0;
    }


    // stats methods
    public boolean doesStatsExist() throws SQLException {
        try {
            Cursor cursor = db.query(DATABASE_TABLE_STATS, new String[]{
                    ROWID, STATS_DATE, STATS_BFPERCENT, STATS_HEIGHT, STATS_WEIGHT, STATS_DAILY_CALS, STATS_DAILY_CARBS, STATS_DAILY_PROTEIN, STATS_DAILY_FAT}, null, null, null, null, null, null);

            cursor.moveToFirst();
            if (1 == (cursor.getCount())) {
                cursor.close();
                return true;
            }
        } catch (Exception e) { //nothing
        }
        return false;
    }


    private long createStat(String date, String bfPercent, String height,
                            String weight, String dailyCals, String dailyCarbs,
                            String dailyProtein, String dailyFat) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(STATS_DATE, date);
        initialValues.put(STATS_BFPERCENT, bfPercent);
        initialValues.put(STATS_HEIGHT, height);
        initialValues.put(STATS_WEIGHT, weight);
        initialValues.put(STATS_DAILY_CALS, dailyCals);
        initialValues.put(STATS_DAILY_CARBS, dailyCarbs);
        initialValues.put(STATS_DAILY_PROTEIN, dailyProtein);
        initialValues.put(STATS_DAILY_FAT, dailyFat);
        return this.db.insert(DATABASE_TABLE_STATS, null, initialValues);
    }

    public StatsModel getAllStats() {
        Cursor c = this.db.query(DATABASE_TABLE_STATS, new String[]{ROWID,
                        STATS_DATE, STATS_BFPERCENT, STATS_HEIGHT, STATS_WEIGHT, STATS_DAILY_CALS, STATS_DAILY_CARBS,
                        STATS_DAILY_PROTEIN, STATS_DAILY_FAT}, null, null, null, null,
                null);
        ArrayList<String> statsList = new ArrayList<>();
        if (c.getCount() > 0) {
            c.moveToFirst();
            for (int i = 1; i < c.getColumnCount(); i++) {
                statsList.add(c.getString(i));
            }
        }
        c.close();
        if (statsList.isEmpty()) {
            return new StatsModel();
        }
        return new StatsModel(statsList.get(0), statsList.get(1), statsList.get(2), statsList.get(3), statsList.get(4), statsList.get(5), statsList.get(6), statsList.get(7));
    }

    public boolean updateStat(long rowId, String date, String bfPercent,
                              String height, String weight, String dailyCals,
                              String dailyCarbs, String dailyProtein, String dailyFat) {
        ContentValues args = new ContentValues();
        args.put(STATS_DATE, date);
        args.put(STATS_BFPERCENT, bfPercent);
        args.put(STATS_HEIGHT, height);
        args.put(STATS_WEIGHT, weight);
        args.put(STATS_DAILY_CALS, dailyCals);
        args.put(STATS_DAILY_CARBS, dailyCarbs);
        args.put(STATS_DAILY_PROTEIN, dailyProtein);
        args.put(STATS_DAILY_FAT, dailyFat);
        return this.db.update(DATABASE_TABLE_STATS, args, ROWID + "=?", new String[]{String.valueOf(rowId)}) > 0;
    }

    // WorkoutLogModel methods
    public long createWorkoutLog(String date, String name, String exerciseName,
                                 String exerciseWeight, String reps, String sets, String lastRoundComplete, String lastPercentageResistance, String lastTimeLimit, String comments) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(WORKOUT_LOG_DATE, date);
        initialValues.put(WORKOUT_LOG_NAME, name);
        initialValues.put(WORKOUT_LOG_EXERCISE_NAME, exerciseName);
        initialValues.put(WORKOUT_LOG_EXERCISE_WEIGHT, exerciseWeight);
        initialValues.put(WORKOUT_LOG_EXERCISE_REPS, reps);
        initialValues.put(WORKOUT_LOG_EXERCISE_SETS, sets);
        initialValues.put(WORKOUT_LOG_EXERCISE_LAST_ROUND_COMPLETE, lastRoundComplete);
        initialValues.put(WORKOUT_LOG_EXERCISE_LAST_PERCENTAGE_RESISTANCE, lastPercentageResistance);
        initialValues.put(WORKOUT_LOG_EXERCISE_LAST_TIME_LIMIT, lastTimeLimit);
        initialValues.put(WORKOUT_LOG_EXERCISE_USER_COMMENTS, comments);

        return this.db.insert(DATABASE_TABLE_WORKOUTLOG, null, initialValues);
    }

    public WorkoutLogModel selectWorkoutLogByDate(String date) {
        Cursor c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_WORKOUTLOG
                + " WHERE " + WORKOUT_LOG_DATE + " = '" + date + "'", null);
        WorkoutLogModel model = new WorkoutLogModel();

        if (c.getCount() == 1) {
            c.moveToFirst();
            model = new WorkoutLogModel(c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8), c.getString(9), c.getString(10));
        }
        c.close();
        return model;
    }

    public boolean deleteWorkoutLogByName(String name) {
        return this.db.delete(DATABASE_TABLE_WORKOUTLOG, WORKOUT_LOG_NAME + "=?", new String[]{name}) > 0;
    }

    public ArrayList<WorkoutLogModel> getAllWorkoutLogs() {
        Cursor c = this.db.query(DATABASE_TABLE_WORKOUTLOG, new String[]{ROWID,
                        WORKOUT_LOG_DATE, WORKOUT_LOG_NAME, WORKOUT_LOG_EXERCISE_NAME,
                        WORKOUT_LOG_EXERCISE_WEIGHT, WORKOUT_LOG_EXERCISE_REPS,
                        WORKOUT_LOG_EXERCISE_SETS, WORKOUT_LOG_EXERCISE_LAST_ROUND_COMPLETE, WORKOUT_LOG_EXERCISE_LAST_PERCENTAGE_RESISTANCE, WORKOUT_LOG_EXERCISE_LAST_TIME_LIMIT, WORKOUT_LOG_EXERCISE_USER_COMMENTS},
                null, null, null, null, null);
        c.moveToFirst();
        ArrayList<WorkoutLogModel> workoutLogModels = new ArrayList<>();
        for (int i = 0; i < c.getCount(); i++) {
            WorkoutLogModel model = new WorkoutLogModel(c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8), c.getString(9), c.getString(10));
            workoutLogModels.add(model);
            c.moveToNext();
        }
        c.close();
        return workoutLogModels;
    }

    public boolean updateWorkoutLog(String date, String name,
                                    String exerciseName, String exerciseWeight, String reps,
                                    String sets, String lastRoundComplete, String lastPercentageResistance, String lastTimeLimit, String comments) {
        ContentValues args = new ContentValues();
        args.put(WORKOUT_LOG_DATE, date);
        args.put(WORKOUT_LOG_NAME, name);
        args.put(WORKOUT_LOG_EXERCISE_NAME, exerciseName);
        args.put(WORKOUT_LOG_EXERCISE_WEIGHT, exerciseWeight);
        args.put(WORKOUT_LOG_EXERCISE_REPS, reps);
        args.put(WORKOUT_LOG_EXERCISE_SETS, sets);
        args.put(WORKOUT_LOG_EXERCISE_LAST_ROUND_COMPLETE, lastRoundComplete);
        args.put(WORKOUT_LOG_EXERCISE_LAST_PERCENTAGE_RESISTANCE, lastPercentageResistance);
        args.put(WORKOUT_LOG_EXERCISE_LAST_TIME_LIMIT, lastTimeLimit);
        args.put(WORKOUT_LOG_EXERCISE_USER_COMMENTS, comments);
        return this.db.update(DATABASE_TABLE_WORKOUTLOG, args, WORKOUT_LOG_DATE + "=?", new String[]{date}) > 0;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        @SuppressWarnings("unused")
        Context activityContext;
        @SuppressWarnings("unused")
        SQLiteDatabase db;
        String[] tableArray = {"Custom_Food", "Users_Data", "Goals", "StatsModel",
                "Food_Log", "Workout_Log", "Exercises", "Programs"};

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.activityContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            this.db = db;
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            for (String aTableArray : tableArray) {
                db.execSQL("DROP TABLE IF EXISTS " + aTableArray);
            }
            onCreate(db);
        }
    }
}
