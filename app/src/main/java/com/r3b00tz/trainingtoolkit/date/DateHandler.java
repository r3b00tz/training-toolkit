package com.r3b00tz.trainingtoolkit.date;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Matthew on 7/16/13.
 */
public class DateHandler
{
    public String getCurrentDateSlashed()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormatMMDDYYYY = new SimpleDateFormat("MMddyyyy");
        StringBuilder nowMMDDYYYY = new StringBuilder(dateFormatMMDDYYYY.format(dateNow));

        nowMMDDYYYY.insert(2, "/");
        nowMMDDYYYY.insert(5, "/");
        return nowMMDDYYYY.toString();
    }
}
