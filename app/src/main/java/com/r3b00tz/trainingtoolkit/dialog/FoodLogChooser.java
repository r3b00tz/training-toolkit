package com.r3b00tz.trainingtoolkit.dialog;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.ExistingFoodItems;

/**
 * Created by Matthew on 7/22/13.
 */
@SuppressLint("ValidFragment")
public class FoodLogChooser extends DialogFragment {
    Context context;
    String date = "";

    @SuppressLint("ValidFragment")
    public FoodLogChooser(Context ctx, String date) {
        // Empty constructor required for DialogFragment
        this.context = ctx;
        this.date = date;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.foodlogchooser, container);

        TextView textExistingItem = (TextView) view.findViewById(R.id.textPreviousItem);
        TextView textAddItem = (TextView) view.findViewById(R.id.textNewItem);

        textExistingItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExistingFoodItems.class);
                intent.putExtra("date", date);
                startActivity(intent);
            }
        });

        textAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }
}