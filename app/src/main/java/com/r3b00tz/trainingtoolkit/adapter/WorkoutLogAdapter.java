package com.r3b00tz.trainingtoolkit.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.PastWorkoutLog;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Matt on 1/29/2017.
 */

public class WorkoutLogAdapter extends RecyclerView.Adapter<WorkoutLogAdapter.ViewHolder> {

    private ArrayList<String> workoutLogNames = new ArrayList<>();
    private ArrayList<String> workoutLogDates = new ArrayList<>();

    public WorkoutLogAdapter(ArrayList<String> names, ArrayList<String> dates) {
        workoutLogNames = names;
        workoutLogDates = dates;
        //reverse so newest is on top
        Collections.reverse(workoutLogDates);
        Collections.reverse(workoutLogNames);
    }

    @Override
    public WorkoutLogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.existing_workout_log_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(WorkoutLogAdapter.ViewHolder holder, final int position) {
        holder.textWorkoutLogName.setText(workoutLogNames.get(position));
        holder.textWorkoutLogDate.setText(workoutLogDates.get(position));
        final String dateSelected = holder.textWorkoutLogDate.getText().toString();
        final String name = holder.textWorkoutLogName.getText().toString();

        holder.workoutLogItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PastWorkoutLog.class);
                intent.putExtra("selectedDate", dateSelected);
                v.getContext().startActivity(intent);
            }
        });

        holder.workoutLogItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Context context = v.getContext();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(context.getResources().getString(R.string.delete_title));
                alertDialogBuilder
                        .setMessage(context.getResources().getString(R.string.delete_message))
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                DBAdapter dbAdapter = new DBAdapter(context);
                                dbAdapter.open();
                                dbAdapter.deleteWorkoutLogByName(workoutLogNames.get(position));
                                dbAdapter.close();

                                workoutLogDates.remove(position);
                                workoutLogNames.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(R.string.cancel,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return workoutLogNames.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CardView workoutLogItem;
        TextView textWorkoutLogName;
        TextView textWorkoutLogDate;

        ViewHolder(View v) {
            super(v);
            workoutLogItem = (CardView) v.findViewById(R.id.workoutLogItem);
            textWorkoutLogName = (TextView) v.findViewById(R.id.textWorkoutLogName);
            textWorkoutLogDate = (TextView) v.findViewById(R.id.textWorkoutLogDate);
        }
    }
}
