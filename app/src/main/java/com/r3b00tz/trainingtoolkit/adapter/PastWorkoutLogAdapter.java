package com.r3b00tz.trainingtoolkit.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;

import java.util.ArrayList;

/**
 * Created by Matt on 1/31/2017.
 */

public class PastWorkoutLogAdapter extends RecyclerView.Adapter<PastWorkoutLogAdapter.ViewHolder> {

    private ArrayList<String> exerciseNames = new ArrayList<>();
    private ArrayList<String> exerciseReps = new ArrayList<>();
    private ArrayList<String> exerciseSets = new ArrayList<>();
    private ArrayList<String> exerciseWeights = new ArrayList<>();

    public PastWorkoutLogAdapter(ArrayList<String> names, ArrayList<String> reps, ArrayList<String> sets, ArrayList<String> weights) {
        exerciseNames = names;
        exerciseReps = reps;
        exerciseSets = sets;
        exerciseWeights = weights;
    }

    @Override
    public PastWorkoutLogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.past_workout_log_list_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PastWorkoutLogAdapter.ViewHolder holder, int position) {
        holder.textExerciseName.setText(exerciseNames.get(position));
        holder.textRepItem.setText(exerciseReps.get(position));
        holder.textSetItem.setText(exerciseSets.get(position));
        holder.textWeightItem.setText(exerciseWeights.get(position));
    }

    @Override
    public int getItemCount() {
        return exerciseNames.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        TextView textExerciseName;
        TextView textRepItem;
        TextView textSetItem;
        TextView textWeightItem;

        ViewHolder(View v) {
            super(v);
            textExerciseName = (TextView) v.findViewById(R.id.textExerciseName);
            textRepItem = (TextView) v.findViewById(R.id.textRepItem);
            textSetItem = (TextView) v.findViewById(R.id.textSetItem);
            textWeightItem = (TextView) v.findViewById(R.id.textWeightItem);
        }
    }
}