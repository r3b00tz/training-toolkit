package com.r3b00tz.trainingtoolkit.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.ExerciseDetail;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;

/**
 * Created by Matt on 2/6/2017.
 */

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ViewHolder> {

    private static ArrayList<String> exerciseNames;

    public ExerciseAdapter(ArrayList<String> dataset) {
        exerciseNames = dataset;
    }

    @Override
    public ExerciseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.exercise_name_list_item, parent, false);

        ExerciseAdapter.ViewHolder vh = new ExerciseAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ExerciseAdapter.ViewHolder holder, final int position) {
        final String name = exerciseNames.get(position);
        holder.mTextView.setText(name);

        holder.card_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ExerciseDetail.class);
                intent.putExtra("editable", false);
                intent.putExtra("exerciseName", name);
                v.getContext().startActivity(intent);
            }
        });

        holder.card_item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Context context = v.getContext();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(context.getResources().getString(R.string.delete_title));
                alertDialogBuilder
                        .setMessage(context.getResources().getString(R.string.delete_message))
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                DBAdapter dbAdapter = new DBAdapter(context);
                                dbAdapter.open();
                                dbAdapter.deleteExerciseByName(exerciseNames.get(position));
                                dbAdapter.close();

                                while(exerciseNames.indexOf(name) != -1) {
                                    exerciseNames.remove(name);
                                }

                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(R.string.cancel,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return exerciseNames.size();
    }

    public void clear() {
        exerciseNames.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<String> list) {
        exerciseNames.addAll(list);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView card_item;
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            card_item = (CardView) v.findViewById(R.id.exerciseNameCard);
            mTextView = (TextView) v.findViewById(R.id.textListItem);
        }
    }
}