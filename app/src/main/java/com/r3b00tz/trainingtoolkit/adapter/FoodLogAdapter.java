package com.r3b00tz.trainingtoolkit.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.r3b00tz.trainingtoolkit.R;
import com.r3b00tz.trainingtoolkit.activities.FoodLog;
import com.r3b00tz.trainingtoolkit.database.DBAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Matt on 2/3/2017.
 */

public class FoodLogAdapter extends RecyclerView.Adapter<FoodLogAdapter.ViewHolder> {

    private ArrayList<String> foodLogDates = new ArrayList<>();

    public FoodLogAdapter(ArrayList<String> dates) {
        foodLogDates = dates;
        //reverse so newest is on top
        Collections.reverse(foodLogDates);
    }

    @Override
    public FoodLogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.existing_food_log_item, parent, false);

        return new FoodLogAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FoodLogAdapter.ViewHolder holder, int position) {

        holder.textFoodLogDate.setText(foodLogDates.get(position));
        final String dateSelected = holder.textFoodLogDate.getText().toString();
        final int pos = position;

        holder.foodLogItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FoodLog.class);
                intent.putExtra("selectedDate", dateSelected);
                v.getContext().startActivity(intent);
            }
        });

        holder.foodLogItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Context context = v.getContext();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(context.getResources().getString(R.string.delete_title));
                alertDialogBuilder
                        .setMessage(context.getResources().getString(R.string.delete_message))
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                DBAdapter dbAdapter = new DBAdapter(context);
                                dbAdapter.open();
                                dbAdapter.deleteFoodLogByDate(foodLogDates.get(pos));
                                dbAdapter.close();

                                foodLogDates.remove(pos);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(R.string.cancel,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return foodLogDates.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CardView foodLogItem;
        TextView textFoodLogDate;

        ViewHolder(View v) {
            super(v);
            foodLogItem = (CardView) v.findViewById(R.id.foodLogItem);
            textFoodLogDate = (TextView) v.findViewById(R.id.textFoodLogDate);
        }
    }
}
